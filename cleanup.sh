#!/bin/bash
find . -name "*~"        -exec rm {} \;
find . -name "*.bbl"     -exec rm {} \;
find . -name "*.bcf"     -exec rm {} \;
find . -name "*.log"     -exec rm {} \;
find . -name "*.xml"     -exec rm {} \;
find . -name "*.backup"  -exec rm {} \;
find . -name "*.lof"     -exec rm {} \;
find . -name "*.glg"     -exec rm {} \;
find . -name "*.blg"     -exec rm {} \;
find . -name "*.gls"     -exec rm {} \;
find . -name "*.toc"     -exec rm {} \;
find . -name "*.glo"     -exec rm {} \;
find . -name "*.lot"     -exec rm {} \;
find . -name "*.out"     -exec rm {} \;
find . -name "*.aux"     -exec rm {} \;
find . -name "*.auxlock" -exec rm {} \;
find . -name "*.run.xml" -exec rm {} \;
find . -name "*.glsdefs" -exec rm {} \;

