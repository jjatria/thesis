\chapter{Testing secondary cues: design and synthesis of materials}
\chaptermark{Synthesis and design of continua}
\label{chp:2_acoustics}

\setcounter{topnumber}{1}
\setcounter{bottomnumber}{1}

\Cref{prt:identification} reported the results of an experiment examining differences in the direction of language learning on the perception of \gls*{L2} lexical prominence.
This was done by looking at the performance of non-native learners listening to naturally-produced target words in different sentence positions and with different syllables as prominent, and repeating the experience with \glsdesc*{js} (\gls*{js}) and \glsdesc*{sj} (\gls*{sj}).

In both cases, conditions were found that allowed for relatively high performances for non-native listeners, showing that differences in the phonological definition of lexical prominence are not enough to predict a low performance in the perception of suprasegmental categories.
Instead, it is more relevant to look at the effect of different contexts in the degree of phonetic similarity between native and non-native categories.
Indeed, differences found between the performance of both language groups showed that the direction of language learning has a strong effect on what contexts will prove easy or difficult for non-native perceivers.
These results support the notion that suprasegmental perception in general, and the perception of second language lexical prominence in particular, is subject to factors that are similar (if not the same) as those involved in perception at a segmental level.

The chapters in \cref{prt:continuum} build on these results by examining whether or not non-native listeners are sensitive to secondary acoustic cues, and if their weighing of these non-native cues is affected by differences in their inventory of \gls*{L1} cues.
Evidence from \cref{chp:1_spanish,chp:1_japanese} suggested this was the case: in both cases, responses from non-native participants could not be entirely explained by the action of \fzero\ alone, with evidence that both groups were basing part of their judgements in the behaviour of intensity and duration as secondary cues.
But a more careful look at this sensitivity was necessary.

\section{Materials}
\label{sec:2_acoustics_materials}

\marginpar{Choosing a secondary cue}%
Between intensity and duration, the latter was chosen as the focal point for the next set of studies because of the evidence that suggests duration has a particular status among acoustic cues, in that it is available as a cue even for listeners who do not natively attend to it~\parencite{bohn1995}.
Additionally, duration has been shown to be more relevant than intensity in the encoding of Spanish prominence in particular~(\cite{ortegallebaria2006, ortegallebaria+2007, ortegallebaria+2008, ortegallebaria+2009}, \cite[and see][for a review]{kim2015}), and across languages in general~\parencite{chun2002}.
And although it is not used for accent discrimination in Japanese, it does have a contrastive role at a segmental level, which increased the likelihood that \gls*{js} would be able to tune in to it.
% TODO(vh) So what are the predictions?
%     (jj) They are in the discussion

Intensity, on the other hand, has no role in accent discrimination in Japanese~\parencite{cutler+1994}; and requires a more involved process of acoustic manipulation, increasing the risk of introducing unwanted variation in the synthetic stimuli.

\subsection{The test paradigm}
\label{sec:2_acoustics_test_paradigm}

To test the hypothesis that duration has an effect on the perception of Spanish and Japanese lexical prominence by non-native listeners, and to measure the degree of this effect, a modified trading relations paradigm~\parencite{nittrouer+1987, mayo+2004, mayo+2005} was used.
This paradigm makes it possible to test the effect of a secondary cue in the presence of a primary one in ways that are resistant to the neutralising effects or that predominant cue.

In general terms, the acceptable variation for the two freely varying target cues ($c_1$ and $c_2$) can be understood to define two independent dimensions which in turn defines a plane.
In this plane, each point corresponds to a specific combination of both cues, and therefore to a different possible stimulus.
Applied in a naïve way, a large number of stimuli would be necessary to cover the entire plane of possible cue combinations.
But this number can be brought down by drastically reducing the resolution along one of these dimensions (\ie~the one corresponding to the secondary variable) and considering only the limits of this cue's variation.
This process is schematically illustrated in \cref{fig:2_acoustics_continuum_plane}.

This process not only reduces the plane to two lines corresponding to the opposite edges of the plane along the dimension of the secondary cue, but also maximises the effect of this cue.
This is a particularly beneficial side-effect, since by definition a secondary cue will have a smaller effect than the primary cue with which it interacts.
This procedure reduces the chances of masking.

There are other methods that also allow to more efficiently sample a complex stimulus space.
One which has provided particularly interesting results is that of sampling by multidimensional vectors \parencite[see][for an example]{iverson+2003}.
While this method remains tempting, the scenario in this case was considered too simple to merit the added complexity of such an approach, which involves adaptive testing, and is more well suited for higher-dimensional spaces.

\begin{figure} % fig:2_acoustics_continuum_plane
  \centering
  \input{\FIGURES/2_acoustics_continuum_plane}%
  \caption[The modified trading relations paradigm]{%
    The plane of possible combinations of $c_1$ and $c_2$ can be reduced to two opposing edges by sacrificing resolution along the secondary cue ($c_{2}$).
  }
  \label{fig:2_acoustics_continuum_plane}
\end{figure}

Points along these edges define the stimuli, each of them representing one step along the continuum of variation for $c_1$ (with $c_2$ fixed at either of its extreme values).
The difference between the two resulting continua will exist solely in $c_2$, so any difference in the responses obtained from these will derive from a sensitivity to this secondary variable, and the magnitude of the difference will correlate with the degree of sensitivity: as sensitivity increases, so will the effect of $c_2$ on participants' responses.
This method allows for a considerable reduction in the number of tested stimuli while still allowing for the precise measure of the contribution of \eg~duration in the presence of pitch differences.
\Cref{fig:2_acoustics_curve_comparison} illustrates this part of the procedure, with the size of the shaded area representing the magnitude of the effect of $c_2$.

\begin{figure} % fig:2_acoustics_curve_comparison
  \centering
  \input{\FIGURES/2_acoustics_curve_comparison}%
  \caption[Stylised response curves for the two extreme continua]{%
    By using the two continua from \cref{fig:2_acoustics_continuum_plane}, participants who are sensitive to $c_2$ will show a spread in their responses.
  }
  \label{fig:2_acoustics_curve_comparison}
\end{figure}

In contrast with the methods used for \cref{prt:identification}, this method offers additional granularity, in that participants' responses are not simply a correct-or-incorrect binary response, or an aggregate of such, but a continuous variable represented by the position of categorical boundaries and the steepness of the response functions.
This additional granularity will become particularly useful in \cref{chp:2_japanese}, when the question of the degree of categorical development in \gls*{sj} participants becomes relevant.

\subsection{Design of the continua}
\label{sec:2_acoustics_continuum design}

To continue from the results in previous studies, the continua used in this case were developed based on a subset of the minimal trios used in \cref{chp:1_spanish,chp:1_japanese}.
Each of the three member words of those trios was arranged according to the position of the prominent syllable, from the antepenult to the ultima.
In the case of Japanese, unaccented words were placed at the end of the continuum based on the similarities of the resulting \fzero\ progression between both languages, the confusion patterns shown in \cref{chp:1_japanese}, which showed Spanish learners more likely to interpret unaccented words as accented on the word's final syllable than on the first%
\footnote{%
  It is important to remember that while target words in Japanese were all 2-syllable words, the stimuli themselves were 3-syllable long, since they included a particle at the end to show the behaviour of the \fzero\ contour after the end of the word (see \hyperref[sec:1_japanese_third_syllable]{\cref*{sec:1_japanese_keywords}}).
  This means that the word's final syllable is the penult of the stimulus.
},
and the steady decline in identification rate for \gls*{sj} participants from initial-accented to unaccented words (see~\cref{fig:1_japanese_accent_effect_trios}).
To simplify the description of the procedure, the following paragraphs will make reference to the members of the trios by their position in this sequence, regardless of the language they belong to.
This means that when reference is made to words in the third or final position, they will refer to oxytone words in Spanish and unaccented words in Japanese.

The sequence of words from the minimal trios were separated into two separate contrasts: one between the first and the second word (\eg~proparoxytone and paroxytone in Spanish), and another between the second and the third word (\eg~final-accented and unaccented words in Japanese).
Each of these contrasts resulted in a separate 10-step continuum, in which each step represented a different equidistant step from the pitch contour of the first term to that of the second.
However, only the variation of the primary cue has been introduced up to this point.

Two versions of each of the 10-step continua were made, each one with the duration values fixed at the position of a different term in the contrast.
In \cref{fig:2_acoustics_continuum_plane}, these two continua correspond to the two sets of stimuli on the simplified plot, each at a different end of the stimulus plane along the dimension of $c_2$.
The continuum using the duration values of the first term was called continuum A, while continuum B had the duration values fixed on those of the final term.
The combination of these two resulted in 4 independent continua:
\begin{enumerate}
  \item[A$_1$] Between the \first\ and the \second\ words, with durations of the \first
  \item[A$_2$] Between the \second\ and the \third\ words, with durations of the \second
  \item[B$_1$] Between the \first\ and the \second\ words, with durations of the \second
  \item[B$_2$] Between the \second\ and the \third\ words, with durations of the \third
\end{enumerate}

\subsection{Source materials}
\label{sec:2_acoustics_source_materials}

Stimuli for the continua were synthesised from recordings made from native speakers of both languages.
The recordings were selected from the set of isolated words from Spanish and Japanese minimal trios that were originally  made for the tests described in \cref{chp:1_spanish,chp:1_japanese} (see \hyperref[sec:1_spanish_speech_materials]{\cref*{chp:1_acoustics}} for details about the recording procedure and the acoustic characteristics of the source materials).
The decision to only use isolated words was based on the results from \cref{chp:1_spanish}, which showed it to be the most beneficial for the perception of \gls*{js} listeners (who reached native-like levels).
This was not the case for \gls*{sj} participants, as was shown in \cref{chp:1_japanese}, but the same results showed that isolated words were among the high-performing phrases, which meant the decision would at the very least not affect \gls*{sj} negatively.
% While ceiling effects might have been problematic for the study in \cref{chp:1_spanish}, maximising the performance of the experimental group is important in a design such as this one, to increase the likelihood of response curves reaching the ends of the scale.
% TODO(jja) Why?
% Because if they are not maximally contrastive, the ends of the response curves will approach a flat line, and then you get nothing.
% You lose.
% Good day, sir.

\section{Digital processing}
\label{sec:2_acoustics_processing}

\begin{figure}[t] % fig:2_spanish_continuum_1
  \centering

  \subfloat[Continuum A$_{1}$ for Spanish]{
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XAvl01}%
    \end{minipage}%
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XAvl10}%
    \end{minipage}
  }

  \subfloat[Continuum B$_{1}$ for Spanish]{
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XBvl01}%
    \end{minipage}%
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XBvl10}%
    \end{minipage}
  }

  \caption[Extremes of continua for the first Spanish contrast]{%
    First and last steps for the continua between proparoxytone and paroxytone words.
    Sounds on the top row have the duration values of a proparoxytone; while those on the bottom row have the duration of a paroxytone.
    The displayed sounds are the female productions of the /balido/ set.
  }
  \label{fig:2_spanish_continuum_1}
\end{figure}

\begin{figure}[t] % fig:2_spanish_continuum_2
  \centering

  \subfloat[Continuum A$_{2}$ for Spanish]{
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XAvl11}%
    \end{minipage}%
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XAvl20}%
    \end{minipage}
  }

  \subfloat[Continuum B$_{2}$ for Spanish]{
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XBvl11}%
    \end{minipage}%
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XBvl20}%
    \end{minipage}
  }

  \caption[Extremes of continua for the second Spanish contrast]{%
    First and last steps for the continua between paroxytone and oxytone words.
    Sounds on the top row have the duration values of a paroxytone; while those on the bottom row have the duration of a oxytone.
    The displayed sounds are the female productions of the /balido/ set.
  }
  \label{fig:2_spanish_continuum_2}
\end{figure}

\marginpar{Manipulation}%
\anchor{sec:2_acoustics_manipulation}%
Manipulations were made using \tandemstraight~\parencite{kawahara+2008,kawahara+2009,kawahara+2011}, a system that improves the spectral calculations originally implemented in \gls*{straight}~\parencite[\glsdesc*{straight};][]{kawahara+1999}.
Both the \gls*{straight} vocoder and its improved version allow for the synthesis and modification of speech signals with very high quality output~\parencite{zen+2007}.

In these systems, speech is defined based on a combination of five attributes, each of which can be manipulated independently:
\begin{enumerate}
 \item An aperiodicity map of the signal
 \item An \fzero\ track, which includes voicing information
 \item[] And three morphing rate parameters defining the interference-free spectrogram:
 \item one for the spectrogram,
 \item one for the time domain,
 \item and one for the frequency domain.
\end{enumerate}

A baseline \fzero\ contour was calculated per utterance and used to remove low frequency noise, improving the accuracy of subsequent measures.
This was further improved by making use of internal functions for \fzero\ auto-tracking and refinement of voiced and unvoiced (\gls*{vuv}) segment detection.
Once frequency data was extracted, the aperiodicity ratio of the sound was calculated and used for the calculation of the \gls*{straight} spectrum.
This series of steps can be seen in \cref{lst:2_acoustics_straight_analysis}, which shows part of the code used to run the analysis in its Matlab implementation.

\begin{lstlisting}[%
    float,
    frame=single,
    framerule=0pt,
    label=lst:2_acoustics_straight_analysis,
    language=Matlab,
    caption={[\gls*{straight} analysis]
    Code used with \tandemstraight\ to automatise the steps in the \gls*{straight} analysis}
  ]
  % Analyse waveform
  % wave and rate are returned from Matlab's wavread
  obj = exF0candidatesTSTRAIGHTGB(wave, rate);
  wave = removeLF(wave, rate, obj.f0, obj.periodicityLevel);
  obj = exF0candidatesTSTRAIGHTGB(wave, rate);
  obj = autoF0Tracking(obj, wave);
  obj.vuv = refineVoicingDecision(wave, obj);
  % Aperiodicity extraction
  apr  = aperiodicityRatioSigmoid(wave, obj, 1, 2, 0);
  % STRAIGHT Spectrum
  spec = exSpectrumTSTRAIGHTGB(wave, rate, apr);
  o.waveform = wave;
  o.samplingFrequency = rate;
  o.refinedF0Structure.temporalPositions = obj.temporalPositions;
  o.SpectrumStructure.spectrogramSTRAIGHT = spec.spectrogramSTRAIGHT;
  o.refinedF0Structure.vuv = obj.vuv;
  spec.spectrogramSTRAIGHT = unvoicedProcessing(o);
\end{lstlisting}

The resulting sound objects were then used to generate the corresponding \emph{morphing substrates}, which are used by \tandemstraight\ for the creation of the continua.
In \tandemstraight, a morphing substrate defines the specific output of the combination of two target sounds based on
\begin{inparablank}
  \item the sounds themselves (and the result of their \gls*{straight} analysis);
  \item a set of time anchors to map corresponding parts of one sound; and
  \item a value for each of the five acoustic attributes described above, specifying whether that attribute should be set at that of either of the target sounds, or somewhere in between along a linear interpolation.
\end{inparablank}

A set of custom scripts (which included the code in \cref{lst:2_acoustics_straight_analysis}) read the time anchors from pre-existing annotations created manually with the software \citetalias{praat}, and handled the manipulation of the five attributes mentioned above.
Within the continuum for each contrast, values for the duration attribute of all steps were set to either those of the initial or final targets, depending on whether the stimulus was from the A or B continua respectively.
Values for \fzero\ were set for each step at one of 10 equidistant points between both target sounds.
The remaining 3 attributes were fixed for all stimuli at a midpoint between both targets.

This explains why the continua between the two contrasts should be considered as separate, and not as two halves of a single continuum: in each case, the midpoint that was the reference value for the remaining attributes was different.
In the case of the continua between the first and the second words the midpoint was between these words, while in the continua for the second contrast the midpoint was between the second and the third words.

\section{The resulting materials}
\label{sec:2_acoustics_resulting_materials}

The manipulation was done on all recordings of isolated minimal trios from all speakers for each language.
The resulting samples were evaluated by native Spanish and Japanese speakers correspondingly to measure their naturalness, and by the researcher to monitor the number of artifacts introduced during the procedure.
\marginpar{Speakers}%
After this selection process, two speakers (one male and one female) were chosen from each language: the older male and female speakers in Spanish, and the younger male and female speakers in Japanese.

\marginpar{Keywords}%
Two minimal trios were chosen for each language, to bring the procedure behind the studies involving each language closer together.
Words from the /balido/ and the /numeɾo/ Spanish sets were used.
These were chosen over the other two minimal sets since they contained only voiced segments, which again resulted in more natural sounding stimuli.
The Japanese stimuli only had two trios, so both of them were used.
The keywords used in this study are shown in \cref{tab:2_spanish_keywords} for Spanish, and \cref{tab:2_japanese_keywords} for Japanese.

\begin{table} %tab:2_spanish_keywords
  \centering
  \input{\TABLES/2_spanish_keywords}
  \caption[Spanish target words]{%
    Spanish target words used by this study.
    Compare with the Japanese keywords shown in \cref{tab:2_japanese_keywords}, on page \pageref{tab:2_japanese_keywords}.
  }
  \label{tab:2_spanish_keywords}
\end{table}

\begin{table} %tab:2_japanese_keywords
  \centering
  \input{\TABLES/2_japanese_keywords}
  \caption[Japanese target words]{%
    Japanese target words used by this study.
    A \fall\ indicates a fall in pitch while a \rise\ indicates a pitch rise.
    \mora\ indicates a mora.
    Compare with the Spanish keywords shown in \cref{tab:2_spanish_keywords}, on page \pageref{tab:2_spanish_keywords}.
  }
  \label{tab:2_japanese_keywords}
\end{table}

Examples of the resulting materials are shown in \crefrange{fig:2_spanish_continuum_1}{fig:2_japanese_continuum_2}.
Each figure shows the first and last step in both the A and B versions of the continuum between one pair of words.
\Cref{fig:2_spanish_continuum_1,fig:2_spanish_continuum_2} show the continua for the first and second contrast for Spanish, while \cref{fig:2_japanese_continuum_1,fig:2_japanese_continuum_2} do so for Japanese.

\begin{figure} % fig:2_japanese_continuum_1
  \centering

  \subfloat[Continuum A$_{1}$ for Japanese]{
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XAkk01}%
    \end{minipage}%
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XAkk10}%
    \end{minipage}
  }

  \subfloat[Continuum B$_{1}$ for Japanese]{
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XBkk01}%
    \end{minipage}%
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XBkk10}%
    \end{minipage}
  }

  \caption[Extremes of continua for the first Japanese contrast]{%
    First and last steps for the continua between initial- and final-accented words.
    Sounds on the top row have the duration values of an initial-accented word; while those on the bottom row have the duration of a final-accented word.
    The displayed sounds are the female productions of the /kaki/ set.
  }
  \label{fig:2_japanese_continuum_1}
\end{figure}

\begin{figure} % fig:2_japanese_continuum_2
  \centering

  \subfloat[Continuum A$_{2}$ for Japanese]{
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XAkk11}%
    \end{minipage}%
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XAkk20}%
    \end{minipage}
  }

  \subfloat[Continuum B$_{2}$ for Japanese]{
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XBkk11}%
    \end{minipage}%
    \begin{minipage}{0.5\linewidth}
      \centering
      \input{\FIGURES/XBkk20}%
    \end{minipage}
  }

  \caption[Extremes of continua for the second Japanese contrast]{%
    First and last steps for the continua between final-accented and unaccented words.
    Sounds on the top row have the duration values of a final-accented word; while those on the bottom row have the duration of an unaccented word.
    The displayed sounds are the female productions of the /kaki/ set.
  }
  \label{fig:2_japanese_continuum_2}
\end{figure}

Although the figures show examples from a single word for a single speaker per language, they provide a reliable illustration of the effects of the manipulation.
No negative effects of stretching were found, and since the start and end points for the manipulation of pitch for each contrast in continua A and B were the same, there was also no difference in \fzero\ range between these two.

\section{Discussion}

This chapter discussed the rationale for the choice of materials for the studies in \cref{chp:2_spanish,chp:2_japanese}, and the procedure followed for their synthesis.
An evaluation of the resulting stimuli by native speakers showed that the results of the manipulation were of very high quality in all cases, a fact that will be corroborated when examining the responses of native speakers when presented with the stimuli in \cref{chp:2_spanish} for Spanish and \cref{chp:2_japanese} for Japanese.
The ease of programmatically defining the steps in each continua also made \tandemstraight\ a very good tool for the job.

This method was suitable in this case since the parameters that were to be manipulated were syllable duration and \fzero, and both of these are directly represented as editable attributes for controlling the calculation of the \gls*{straight} spectrum needed for synthesis.
In tasks in which the objective is the careful manipulation of the formant structure of speech sounds, for example, a tool such as \tandemstraight\ might not prove as convenient to use, even if such manipulations are possible.
In the case of this study, the fact that \tandemstraight\ did not offer a convenient interface for intensity manipulation had a definite role to play in the decision to prioritise the study of the effects of duration.
In choosing a tool such as \tandemstraight, a decision was made to prioritise the naturalness of the results over the minute control of the aspects that were changing in each step that could be achieved with other methods like a Klatt synthesizer~\parencite{klatt1987, klatt+1990}.
Whether this decision was worth the costs is up to the reader for consideration.

\subsection{The Spanish stimuli}
\label{sec:2_acoustics_discussion_spanish}

Differences in the syllable duration of the original stimuli, described in detail in \cref{sec:1_spanish_duration} and shown in \cref{fig:2_spanish_duration_ratios_per_accent}, resulted in significant differences between the A and B continua, and therefore in suitable stimuli to highlight the sensitivity to duration as a secondary cue for the perception of \gls*{L2} prominence.
Results from \cref{chp:1_spanish} showed that non-native participants are very capable of correctly perceiving the position of the prominence in Spanish words in isolation, and that they are furthermore sensitive to duration as a cue.
Based on this, \gls*{js} are predicted to show response curves that differ between the A and the B continua, and to show similar categorical boundaries to those exhibited by native speakers.
A more detailed break down of the proportion of the word that belonged to each syllable, for each of the original tokens used as the source for the manipulations, is shown in \cref{tab:2_spanish_speaker_durations,tab:2_japanese_speaker_durations}, in \hyperref[chp:appendix_duration_tables]{the appendix}.

\begin{figure}
  \centering
  \input{\FIGURES/2_japanese_duration_ratios_per_accent}%
  \caption[Distribution of word duration per syllable in Japanese]{%
    Distribution of the duration of Japanese isolated words per syllable for initial- and final-accented words, and for unaccented words.
    The \showcolor{interval} line marks the point at which all syllables would have the same length.
    (\mean$\pm$\sd).
  }
  \label{fig:2_japanese_duration_ratios_per_accent}
\end{figure}

\begin{figure}
  \centering
  \input{\FIGURES/2_spanish_duration_ratios_per_accent}%
  \caption[Distribution of word duration per syllable in Spanish]{%
    Distribution of the duration of Spanish isolated words per syllable for each of the three positions of the prominence.
    The \showcolor{interval} line marks the point at which all syllables would have the same length.
    (\mean$\pm$\sd).
  }
  \label{fig:2_spanish_duration_ratios_per_accent}
\end{figure}

This is the case particularly with the first contrast (continua A$_1$ and B$_1$).
The lower performance shown by \gls*{js} participants with oxytone words even in isolated contexts (shown in \cref{fig:1_spanish_confusion_per_question_position}) mean that the continua for the second contrast will likely elicit fewer correct responses, particularly towards the oxytone end of the scale.
This relatively low performance with prominent syllables towards the end of the word was explained in \cref{sec:1_spanish_discussion_acoustic_cues} with reference to the effects of duration, and in particular to those of the older male speaker.
If this is indeed the case, then it should be clearly reflected on the responses to these stimuli, since half of them come from the speaker in question.
% If the perception of later prominent syllables is indeed affected primarily by duration, then responses for stimuli from the male speaker in particular will reach lower levels of category recognition than those from the female speaker; and this effect should be stronger for the second contrast than for the first.
% TODO (jj) Did not look at speaker differences

In a design like the one used here, there is also a clear possibility that stimuli at the extremes of the continua will suffer more from the conflict that exists between cues from different words.
In this case, this conflict will be greatest for stimuli at the beginning of the B continua and the end of the A continua.
Some degree of additional difficulty might become apparent for groups that are particularly sensitive to changes in duration.

\subsection{The Japanese stimuli}
\label{sec:2_acoustics_discussion_japanese}

In the case of the Japanese stimuli, the little duration variation in the original stimuli described in \cref{sec:1_japanese_duration}, and shown in \cref{fig:2_japanese_duration_ratios_per_accent}, resulted in a great similarity between the stimuli from continuum A and B.
However, \cref{chp:1_japanese} suggested an effect of secondary cues even under these conditions, an effect that was explained as an over-sensitivity to random variations in intensity and duration outside of the control of the native speakers.
If that is the case, then what matters is not so much the duration variation in the production of native speakers, but the heightened sensitivity to duration changes in the non-native listeners; and duration is known to be a highly significant cue for the perception of Spanish prominence~\parencite{ortegallebaria2006}.

A difference in the responses for both continua, in the face of the small differences apparent in the original stimuli, would strongly support the conclusions of \cref{chp:1_japanese} as they relate to duration; while the lack of such a difference would suggest that the effect is either too small for this design to reveal it, or lie more on intensity differences than in those of duration.

In the case of the Japanese study, a more pressing question relates to the degree of categorical development in \gls*{sj} participants.
Results from \cref{chp:1_japanese} showed that non-native performance for Japanese unaccented words was particularly low, with participants reaching chance level responses in both minimal trios and pairs.
The task in that occasion was one of identification, for which some knowledge of the categories to be identified is a requirement.
In this case, the low performance was explained as the lack of that knowledge, and in particular the poor development of an appropriate unaccented category.

If this is the case, \gls*{sj} participants would be expected to show differences in the category slopes for the first and the second contrast, with category slopes between accented categories being much greater than those between accented and unaccented words.

\fleuron

\setcounter{topnumber}{2}
\setcounter{bottomnumber}{2}
