\chapter{\texorpdfstring%
  {Effects of duration on the perception of Spanish as L$2$}%
  {Effects of duration on the perception of Spanish as L2}%
}
\chaptermark{Spanish prominence: effects of duration}
\label{chp:2_spanish}

\blankfootnote{Digital materials related to this study can be found in the study's repository at \url{http://www.pinguinorodriguez.cl/research/phd/secondary-cues/spanish}.}%

\Cref{chp:2_acoustics} explained the motivation of a set of studies on the sensitivity to non-native secondary acoustic cues and the development of \gls*{L2} accentual categories, and provided details about the preparation of the speech materials to be used in those studies.
This chapter reports the results of the application of this methodology to the case of \glsdesc*{js} (\gls*{js}).

\section{Design}

As explained in the previous chapter, the studies in \cref{prt:continuum} focus on two main research questions:
\begin{enumerate}
  \item Does duration have an effect on the perception of lexical prominence by non-native learners?
  \item Do non-native participants show effects of their \gls*{L1} in the weighing of secondary \gls*{L2} acoustic cues?
\end{enumerate}

\marginpar{Research variables}%
Two groups of synthetic continua were generated to be used as the stimuli for this study.
The groups had the syllable durations of the words at opposing ends of the continuum, such that continua labelled ``A'' always had syllables with the duration of those in the first term, and those labelled ``B'' always had the syllable duration of the second.
Based on the test paradigm described in \cref{sec:2_acoustics_test_paradigm}, the sensitivity to changes in duration would be operationalised the magnitude of the shift in the response curves for both continua.
The magnitude of the category slopes, on the other hand, would provide information on the definition of those categories.

\marginpar{Linguistic questionnaire}%
To facilitate the comparison between the results of this experiment, and those of the study reported in \cref{chp:1_spanish}, participants were presented with a preliminary linguistic questionnaire which contained questions about their linguistic background, as well as their self-reported levels of \gls*{L2} proficiency and use.
More details about the preparation and suitability of this questionnaire are covered in \cref{sec:measuring_proficiency}.

\subsection[Participants]{\texorpdfstring%
  {Participants%
  \footnote{Special thanks to Hernán Acuña in Concepción; and Misae Kunitake and Kazuaki Yasuki in \Tokyo\ for assistance with participant recruitment and data collection.}}%
  {Participants}
}
\label{sec:2_spanish_participants}

\marginpar{Japanese participants: \gls*{js}}%
A total of 24 (21 female and 3 male) Japanese participants between the ages of 18 and 32 (\mean{21.7}{3.3}) were recruited from the student body enrolled in the 4-year Spanish programs at Sophia University, \Tokyo\ University of Foreign Studies, and Waseda Universities, all of them in \Tokyo.

\Cref{fig:2_spanish_proficiency} shows the distribution of participants according to the three self-reported measures of second language proficiency: length of studies, overall proficiency, and frequency of use.
Based on their accounts, most participants were in a beginner-to-intermediate level (\mean{2.25} in a 5-point scale; \sd{0.86}), and used their \gls*{L2} on average close to once per week (\mean{2.73}{0.84}, with $3$ meaning once a week).

\begin{figure} % fig:2_spanish_proficiency
  \centering
  \input{\FIGURES/2_spanish_proficiency}%
  \caption[Self-reported measures of proficiency of \gls*{js} participants]{%
    Self-reported measures of \gls*{L2} proficiency of \gls*{sj} participants.
    The horizontal position of each circle marks the mean score reported by each participant to the three scales.
    In the case of proficiency and use, participants were given a 5-point scale, while they were free to enter how many years they had studied for.
    The area of each circle reflects the number of participants for that particular score.
  }
  \label{fig:2_spanish_proficiency}
\end{figure}

\marginpar{Chilean participants}%
Chilean controls were recruited from the city of Concepción, in the south of the country, and tested at the local Universidad de Concepción.
A total of 35 (18 female and 17 male) participants from Chile between 17 and 32 years old (\mean{21.6}{3.2}) participated in the test.

\subsection{The test procedure}
\label{sec:2_spanish_test_procedure}

 The test was separated into three stages:
\begin{enumerate}
  \item The linguistic questionnaire mentioned above.
  \item A practice stage, during which participants were shown examples of the questions and given feedback on their answers.
  \item A testing stage, with 6 different testing blocks separated by breaks.
\end{enumerate}
Participants wore headphones and took the test in a quiet room under experimenter supervision.
Each testing session (including all 3 stages and the breaks) lasted \textasciitilde45 minutes in total.

\marginpar{Local testing}%
In an attempt to increase the number of participants, the studies reported in \cref{prt:identification} used an open registration process in which participants were allowed to take the test unsupervised, and using any computer connected to the internet.
However, this did not result in the large number of participants that were anticipated.

As a result, and to prevent some of the methodological issues entailed by that approach (and discussed in \hyperref[sec:1_spanish_design]{\cref*{sec:1_spanish_test_procedure}}), participants in the current study were asked to register prior to the test and attend one of many supervised testing sessions hosted locally.
Despite these changes, the same testing platform used for \cref{prt:identification}, which allowed for both local and remote testing, was used here as well.

Each session took place in a quiet room with up to 15 participants at a time.
All participants were provided with headphones which they were asked to wear during the test.
They were allowed to set the loudness of the test to a comfortable level before starting.

\marginpar{Presentation}%
The 40 ($ 2 \times 2 \times 10 $) tokens resulting from the synthesis explained in \cref{chp:2_acoustics} were compressed as 192\kbps\ \gls*{mp3} files using \gls*{lame}%
\footnote{%
  Version 3.99.5, available at \url{http://lame.sourceforge.net/}.
  Encodings were made using its \texttt{-h} flag, which ensures the highest quality available.%
}
to limit the transmission rate from the test server without sacrificing too much of the original quality of the recordings.
All tokens were presented once per testing block for a total of 240 stimuli.

The presentation of the stimuli was identical to that used in the study reported in \cref{chp:1_spanish,chp:1_japanese}, and described in \hyperref[sec:1_spanish_question]{\cref*{sec:1_spanish_question}}: after listening to each token, participants were presented with three buttons labelled with the individual items in the trio to which the word belonged.
From these, they were asked to choose the one corresponding to the word they thought they had heard.
The only difference was that in this case the stimuli were all in isolation and had been synthetically manipulated.
The recorded sound played automatically each time the question loaded, but participants were allowed to replay it up to 2 times (see \hyperref[sec:replay_button]{\cref*{sec:replay_button}}).

As in the previous study, all instructions and button labels was presented using the standard orthography of the target language (in this case, Spanish).

\marginpar{The practice stage}%
Before going into the testing state, participants were shown sets of six natural training items from two speakers in a random order.
Items used for this stage came from the same speakers used for the real stimuli, but were selected from the unused recordings to ensure that they were not part of the testing stage to prevent effects of habituation.
The presentation of the practice items was identical to the one described above, with the exception of additional feedback presented to the participants telling them whether their response had been correct or not.

After going through all six items once, participants who had incorrectly identified one or more items would be told to go through the practice session again.
They could do this up to four times.
During this time, they were encouraged to ask questions should any part of the test procedure be unclear.
Only participants who successfully identified all six natural items in any of the attempts were allowed to continue.

\marginpar{Randomisation}%
Stimuli within each testing block were presented in a pseudo-random order that was generated anew for each participant.
The randomisation of the stimuli was done in two steps, to accommodate for limitations in the software that was chosen to run the tests.

\begin{enumerate}
  \item During the first step, speakers were randomly assigned to each of the 6 repetitions of each token across testing blocks, so that half of the repetitions were produced by each speaker.
  This assignment remained constant for all participants

  \item The second step, which took place per participant, determined the order in which each stimulus would appear within each testing group.
\end{enumerate}
All participants heard each stimulus in the same sequence of speakers across all testing groups; but the position of each stimulus in each testing block was different per participant.
A graphical representation of this scheme is shown in \cref{fig:2_spanish_randomisation_scheme}.

\begin{figure}
  \centering
  \input{\FIGURES/2_spanish_randomisation_scheme}%
  \caption[Stimuli randomisation scheme]{%
    The stimuli randomisation scheme.
    A sequence of speakers is first chosen for each stimulus across testing blocks, and the order of stimuli within in each block is later randomised per participant.
    The resulting sequence is shown on the right.
    Numbers represent individual stimuli; colours represent speakers.
  }
  \label{fig:2_spanish_randomisation_scheme}
\end{figure}

\section{Results}
\label{sec:2_spanish_results}

Responses were analysed using probit analysis~\parencite{nittrouer+1987, mayo+2004, mayo+2005} on the responses of each participant for each of the two contrasts: first between proparoxytone and paroxytone words, and then between paroxytone and oxytone.

The fitted probit models were then used to predict the position of the category boundary and slope for each participant.
When a participant's responses for a given category did not cross the 50\percent\ mark, they were marked as having no boundaries.
Category slopes were derived from the model coefficients, indicating the rate of change of the identification function.
The higher the value of the slope the steeper the response curve is.
A response curve with slopes close to zero results from a participant not identifying the terms in the contrast as members of different categories.

Values for category boundaries and slopes per participant became the dependent variables in separate 3-way \gls*{anova}s, using the response category, the language group (native or non-native), and the step in the continua as the predictors.
All tests were run using the \citetalias{r} statistics package.

To examine the possible effects of \gls*{L2} proficiency on the task, an aggregate measure was calculated for the participants' self-reported language use and proficiency by averaging across the multiple ratings given for each in the linguistic questionnaire (see \cref{sec:measuring_proficiency} for details).
The \gls*{anova}s were repeated excluding native participants (and therefore the language group predictor) and including these measures as predictors.

Overall results for both continua are shown in \cref{fig:2_spanish_results_fst-mid,fig:2_spanish_results_mid-lst}, for the first and the second contrasts respectively.
The figures show the percentage of responses for each category at every step in the continua, for both native and non-native listeners.
Results in the figure are averaged across participant responses for both speakers.
In each figure, the difference between the plots on the left and those on the right reflects the effect of the manipulations in syllable duration.

\subsection[First contrast]{\texorpdfstring%
  {First contrast: proparoxytone \versus\ paroxytone}%
  {First contrast}%
}

\Cref{fig:2_spanish_results_fst-mid} shows the responses for the first contrast.
Stimuli that were manipulated to have the duration of paroxytone words (on the right side of the figure) show a distribution of responses that is close to the canonical distribution in studies of this design, with high percentages of contrasting responses in opposite extremes of the continuum.
Stimuli that had the syllable durations of proparoxytone words, on the other hand, showed much shallower slopes and a much greater number of participants whose responses did not cross the 50\percent\ threshold.
This was true for both the native and the non-native groups.

\begin{figure}
  \centering
  \input{\FIGURES/2_spanish_results_fst-mid}%
  \caption[Averaged response functions for the first contrast]{%
    Percentage of responses per category at each step in the continua for the first contrast across all participants and speakers.
    Stimuli with paroxytone and oxytone responses are shown in the left and right columns respectively, while rows separate responses between language groups.
    The \showcolor{chance} line marks the chance level.
    Compare with Japanese results in \cref{fig:2_japanese_results_fst-mid}, on page \pageref{fig:2_japanese_results_fst-mid}.
  }
  \label{fig:2_spanish_results_fst-mid}
\end{figure}

\subsubsection{Category boundaries}

%                           Df Sum Sq Mean Sq F value Pr(>F)
% continuum                  1 139.17  139.17  95.940 <2e-16 ***
% group                      1   0.43    0.43   0.298  0.586
% category                   1   2.90    2.90   1.997  0.160
% continuum:group            1   1.76    1.76   1.215  0.272
% continuum:category         1   0.40    0.40   0.278  0.599
% group:category             1   0.00    0.00   0.001  0.978
% continuum:group:category   1   0.06    0.06   0.043  0.836
% Residuals                156 226.30    1.45


\marginpar{Sensitivity to duration}%
Results showed a strong effect of syllable duration across groups, with the difference between the boundary positions being statistically significant (\fvalue{1,156}{95.94}{<0.001}).
As expected, stimuli with the duration of proparoxytone words resulted in more proparoxytone responses and later boundary positions (\mean{7.01}{1.75}) than those of the stimuli with the paroxytone durations (\mean{5.31}{2.06}).

No other significant effects on boundary positions were found for this contrast.
This was likely due to the number of cases in which no category boundaries could be determined: out of the 144 measures taken from \gls*{js} participants ($24 \times 2 \times 3$), only 75 (52.08\percent) had category boundaries; and similar results were obtained for the 35 native participants (89 out of 210 measures, or 42.38\percent).
However, these numbers include responses for all categories, and proparoxytone responses were expected to be low because that category was not part of this contrast.

\subsubsection{Category slopes}

%                           Df Sum Sq Mean Sq F value   Pr(>F)
% continuum                  1   0.46   0.455   0.528   0.4678
% group                      1   1.59   1.593   1.850   0.1747
% category                   2  50.24  25.118  29.166 2.02e-12 ***
% continuum:group            1   0.40   0.404   0.469   0.4940
% continuum:category         2   7.81   3.906   4.536   0.0114 *
% group:category             2   4.81   2.407   2.795   0.0625 .
% continuum:group:category   2   2.93   1.463   1.699   0.1845
% Residuals                342 294.53   0.861

\marginpar{Main effect of accent category}%
As expected from \cref{fig:2_spanish_results_fst-mid}, analysis of category slopes in this contrast revealed significant differences between accent categories (\fvalue{2,342}{29.17}{<0.001}), with mean values for the response functions for proparoxytone (\mean{-0.43}{0.84}) and paroxytone (\mean{0.49}{0.94}) being significantly greater than those for oxytone words (\mean{0.11}{1.04}).
This was expected, since in both contrasts one of the categories that the participants were presented with was not present: it was a 3\gls*{afc} task with 2-way contrasts.
Because of this, participants were expected to have low responses overall for this \emph{third} category, and therefore flat response functions; and this is what was found in this case.
\Cref{fig:2_spanish_slopes_category_fst-mid} shows the results for slopes per category.

\begin{figure} % fig:2_spanish_slopes_category_fst-mid
  \centering
  \subfloat[By continuum and category, first contrast]{%
    \label{fig:2_spanish_slopes_continuum_category_fst-mid}
    \input{\FIGURES/2_spanish_slopes_continuum_category_fst-mid}%
  }\\
  \subfloat[By language group and category, first contrast]{%
    \label{fig:2_spanish_slopes_group_category_fst-mid}
    \input{\FIGURES/2_spanish_slopes_group_category_fst-mid}%
  }%
  \caption[Category interactions for response slopes]{%
    Distribution of category slope values per category and language group \subref{fig:2_spanish_slopes_group_category_fst-mid} and by continuum \subref{fig:2_spanish_slopes_continuum_category_fst-mid} in the first contrast.
    The \showcolor{interval} line indicates a flat response function.
    Numbers above each box indicate the corresponding $n$.
  }
  \label{fig:2_spanish_slopes_category_fst-mid}
\end{figure}

\marginpar{Interaction between category and continuum}%
A significant interaction between category and continuum was also found (\fvalue{2,342}{4.54}{<0.05}), with the difference in the slope of the response curves of both continua being greater for paroxytone words ($0.39$) than for proparoxytone words ($0.32$).
On the other hand, the difference between the slopes of the two \emph{active} categories (\ie~proparoxytone and paroxytone) was greater for native listeners ($1.09$) than for \gls*{js} listeners ($0.67$), but this difference was just outside significance (\fvalue{2,342}{2.8}{=0.062}).
\Cref{fig:2_spanish_slopes_category_fst-mid} shows the results for both of these interactions per separate.

\subsubsection{\texorpdfstring%
  {\Gls*{L2} proficiency}%
  {L2 proficiency}%
}

% Boundaries
%
%                             Df Sum Sq Mean Sq F value   Pr(>F)
% prof                         1   1.10    1.10   0.918    0.342
% use                          1   0.00    0.00   0.004    0.949
% prof:use                     1   0.56    0.56   0.466    0.497
% prof:category                1   0.09    0.09   0.072    0.789
% use:category                 1   0.12    0.12   0.097    0.757
% prof:continuum               1   0.05    0.05   0.041    0.841
% use:continuum                1   0.09    0.09   0.075    0.785
% prof:use:category            1   0.57    0.57   0.481    0.491
% prof:use:continuum           1   0.50    0.50   0.421    0.519
% prof:category:continuum      1   0.21    0.21   0.177    0.676
% use:category:continuum       1   0.03    0.03   0.025    0.874
% prof:use:category:continuum  1   0.25    0.25   0.206    0.652
% Residuals                   59  70.42    1.19

% Slopes
%
%                              Df Sum Sq Mean Sq F value   Pr(>F)
% prof                          1   1.15   1.154   1.849 0.176492
% use                           1   0.71   0.711   1.139 0.288014
% prof:use                      1   0.05   0.048   0.077 0.781886
% prof:category                 2   3.11   1.553   2.487 0.087414 .
% use:category                  2   1.73   0.866   1.387 0.253737
% prof:continuum                1   0.04   0.039   0.063 0.802131
% use:continuum                 1   0.00   0.003   0.005 0.942593
% prof:use:category             2   0.24   0.120   0.193 0.824812
% prof:use:continuum            1   1.37   1.372   2.198 0.140821
% prof:category:continuum       2   0.73   0.367   0.588 0.557206
% use:category:continuum        2   0.05   0.026   0.042 0.959147
% prof:use:category:continuum   2   2.82   1.410   2.259 0.108929
% Residuals                   120  74.93   0.624

\marginpar{Only marginal interaction:\\category$\times$proficiency}%
No significant main effects of either measure of \gls*{L2} proficiency were found for category boundaries or slopes.
In the case of slopes, an interaction between category and proficiency was found to be just outside the threshold of significance (\fvalue{2,120}{2.49}{0.087}).

\subsection[Second contrast]{\texorpdfstring%
  {Second contrast: paroxytone \versus\ oxytone}%
  {Second contrast}%
}

\subsubsection{Category boundaries}

%                          Df Sum Sq Mean Sq F value   Pr(>F)
% continuum                  1  284.5  284.49 164.134  < 2e-16 ***
% group                      1   75.1   75.12  43.339 3.18e-10 ***
% category                   2  132.0   66.02  38.089 5.56e-15 ***
% continuum:group            1    0.2    0.20   0.115 0.734533
% continuum:category         1   20.1   20.12  11.609 0.000777 ***
% group:category             2   10.8    5.42   3.127 0.045740 *
% continuum:group:category   1    0.0    0.01   0.004 0.946785
% Residuals                226  391.7    1.73

\marginpar{Sensitivity to duration}%
Responses in the second contrast, shown in \cref{fig:2_spanish_results_fst-mid}, reveal a smaller (but still highly significant; \fvalue{1,226}{164.13}{<0.001}) main effect of syllable duration.
In this case, stimuli with the duration of paroxytones (in continuum A$_2$) had later boundary positions (\mean{6.24}{1.48}) than those with the duration of oxytones (in continuum B$_2$; \mean{4.04}{1.77}).

\begin{figure}
  \centering
  \input{\FIGURES/2_spanish_results_mid-lst}%
  \caption[Averaged response functions for the second contrast]{%
    Percentage of responses per category at each step in the continua for the second Spanish contrast across all participants and speakers.
    Stimuli with paroxytone and oxytone responses are shown in the left and right columns respectively, while rows separate responses between language groups.
%     The \showcolor{interval} dashed line shows the sum of the averaged responses for proparoxytone and paroxytone responses.
    The \showcolor{chance} line marks the chance level.
    Compare with Japanese results in \cref{fig:2_japanese_results_mid-lst}, on page \pageref{fig:2_japanese_results_mid-lst}.
  }
  \label{fig:2_spanish_results_mid-lst}
\end{figure}

\marginpar{Effects of category and group}%
A main effect of category was also significant (\fvalue{2,226}{38.09}{<0.001}), with mean boundary positions for paroxytone words occurring earlier (\mean{4.84}{1.97}) than those for oxytone words (\mean{5.88}{1.64}).
Meanwhile, a significant main effect of language group was also found (\fvalue{1,226}{43.34}{<0.001}), with category boundaries for non-native listeners (\mean{5.71}{2.06}) occurring closer to the end of the word than for native listeners (\mean{4.66}{1.8}) across continua.

These two main effects also had a significant interaction (\fvalue{1,226}{3.13}{<0.05}).
\Cref{fig:2_spanish_boundaries_group_category_mid-lst} shows the results for this effect, and illustrates that while boundary positions for paroxytones were similar for both language groups, \gls*{js} listeners placed the boundary of their oxytone category significantly later in the continua.

\begin{figure} % fig:2_spanish_boundaries_group_category_mid-lst
  \centering
  \input{\FIGURES/2_spanish_boundaries_group_category_mid-lst}%
  \caption[Category boundaries per group and accent category]{%
    Distribution of category boundary positions per group and accent category for the second Spanish contrast.
    Numbers above each box indicate the corresponding $n$, after discounting participants for which boundary positions could not be estimated.
  }
  \label{fig:2_spanish_boundaries_group_category_mid-lst}
\end{figure}

\marginpar{Effects of duration per category}%
This difference between the boundary positions of accentual categories was also susceptible to changes in syllable duration, as shown by the significant interaction between continuum and category (\fvalue{1,226}{11.61}{<0.001}).
These results are shown in \cref{fig:2_spanish_boundaries_continuum_category_mid-lst}, which shows that the change in boundary position was greater for paroxytone ($2.61$) than for oxytone responses ($1.46$).

\subsubsection{Category slopes}

%                           Df Sum Sq Mean Sq F value   Pr(>F)
% continuum                  1   0.00   0.002   0.081  0.77584
% group                      1   0.02   0.017   0.695  0.40505
% category                   2  41.88  20.940 871.732  < 2e-16 ***
% continuum:group            1   0.04   0.043   1.803  0.18024
% continuum:category         2   0.60   0.298  12.424 6.18e-06 ***
% group:category             2   0.90   0.452  18.808 1.78e-08 ***
% continuum:group:category   2   0.25   0.123   5.114  0.00648 **
% Residuals                342   8.22   0.024

\marginpar{Different slopes per category}%
Slopes of the response functions of different accentual categories were found to be significantly different for this contrast (\fvalue{2,342}{871.73}{<0.001}), with the oxytone category having the steepest mean slope (\mean{0.47}{0.18}), followed by that of paroxytones (\mean{-0.32}{0.18}).
The slope of proparoxytone responses was much smaller (\mean{-0.19}{0.15}), but still greater than the value found for the first contrast, since in this case there were a considerable number of responses for the \emph{absent} category, particularly in the first half of the B$_2$ continuum.

\marginpar{Effects of duration per accentual category}%
No main effect of the duration manipulation was found for category slopes, but the difference alluded to in the previous paragraph did result in a significant 2-way interaction between the continuum and the accentual category (\fvalue{2,342}{12.42}{<0.001}).
\Cref{fig:2_spanish_slopes_continuum_category_mid-lst} shows that when syllable duration was that of paroxytone words, proparoxytone responses had slopes that were indistinguishable from zero (\mean{-0.14}{0.14}), but a shift in syllable duration to that of oxytone words (in continuum B$_2$) caused the magnitude of the response function slope to increase significantly (\mean{-0.26}{0.12}).

\begin{figure} % fig:2_spanish_boundaries_continuum_category_mid-lst
  \centering
  \input{\FIGURES/2_spanish_boundaries_continuum_category_mid-lst}%
  \caption[Boundaries for continuum and category, second contrast.]{%
    Differences in the effect of duration manipulations on boundary positions by accent category for the second Spanish contrast.
    Numbers above each box indicate the corresponding $n$, after discounting participants for which boundary positions could not be estimated.
  }
  \label{fig:2_spanish_boundaries_continuum_category_mid-lst}
\end{figure}

\marginpar{Differences between categories and language groups}%
Another significant 2-way interaction was found between accent category and language group (\fvalue{2,342}{18.81}{<0.001}).
The driving factor in this case was the difference in paroxytone slopes for the native listeners (\mean{-0.38}{0.17}), which were significantly greater than those of the \gls*{js} listeners (\mean{-0.22}{0.14}).
More detailed results for this interaction are shown in \cref{fig:2_spanish_slopes_group_category_mid-lst}.

\begin{figure} % fig:2_spanish_slopes_category_mid-lst
  \centering
  \subfloat[By continuum and category, second contrast]{%
    \label{fig:2_spanish_slopes_continuum_category_mid-lst}
    \input{\FIGURES/2_spanish_slopes_continuum_category_mid-lst}%
  }\\
  \subfloat[By language group and category, second contrast]{%
    \label{fig:2_spanish_slopes_group_category_mid-lst}
    \input{\FIGURES/2_spanish_slopes_group_category_mid-lst}%
  }%
  \caption[Category interactions for response slopes]{%
    Distribution of category slope values per category and language group \subref{fig:2_spanish_slopes_group_category_mid-lst} and by continuum \subref{fig:2_spanish_slopes_continuum_category_mid-lst} in the second contrast.
    The \showcolor{interval} line indicates a flat response function.
    Numbers above each box indicate the corresponding $n$.
  }
  \label{fig:2_spanish_slopes_category_mid-lst}
\end{figure}

\marginpar{Three-way interaction:\\category$\times$group$\times$cont.}%
Part of the variance of these two interactions is explained by the presence of a significant 3-way interaction between accentual category, language group, and continuum (\fvalue{2,342}{5.11}{<0.01}), which is shown in \cref{fig:2_spanish_slopes_continuum_group_category_mid-lst}.
The main difference in this case is the large increase in the mean slopes for native proparoxytone responses between those for stimuli with the duration of paroxytones (\mean{-0.08}{0.11}), and those for stimuli with the duration of oxytones (\mean{-0.28}{0.12}).

\begin{figure} % fig:2_spanish_slopes_continuum_group_category_mid-lst
  \centering
  \input{\FIGURES/2_spanish_slopes_continuum_group_category_mid-lst}%
  \caption[Category slopes per category in second contrast]{%
    Change in category slopes for each category between continua for the between paroxytone and oxytone words.
    Higher values indicate the rate of change per step in the continuum.
    The \showcolor{interval} line indicates a flat response function.
    ($n=$59/box).
  }
  \label{fig:2_spanish_slopes_continuum_group_category_mid-lst}
\end{figure}

\subsubsection{\texorpdfstring%
  {\Gls*{L2} proficiency}%
  {L2 proficiency}%
}

% Boundaries
%                             Df Sum Sq Mean Sq F value   Pr(>F)
% prof                         1   9.15    9.15   5.594   0.0207 *
% use                          1   6.29    6.29   3.847   0.0537 .
% prof:use                     1   3.77    3.77   2.303   0.1334
% prof:category                2   9.28    4.64   2.838   0.0650 .
% use:category                 2   2.13    1.06   0.651   0.5246
% prof:continuum               1   0.76    0.76   0.463   0.4983
% use:continuum                1   3.55    3.55   2.168   0.1452
% prof:use:category            2   2.75    1.37   0.840   0.4359
% prof:use:continuum           1   2.61    2.61   1.598   0.2102
% prof:category:continuum      1   1.15    1.15   0.705   0.4038
% use:category:continuum       1   1.79    1.79   1.095   0.2989
% prof:use:category:continuum  1   4.74    4.74   2.898   0.0930 .
% Residuals                   73 119.42    1.64

\marginpar{Weak effects of proficiency and use}%
In this contrast, unlike the previous one, a weak main effect of proficiency on category boundaries was found (\fvalue{2,73}{5.49}{<0.05}), with participants who were more proficient showing a small tendency to have earlier boundaries.
%
% Slopes
%
%                              Df Sum Sq Mean Sq F value  Pr(>F)
% prof                          1  0.001   0.001   0.060 0.80640
% use                           1  0.005   0.005   0.213 0.64521
% prof:use                      1  0.007   0.007   0.293 0.58910
% prof:category                 2  0.066   0.033   1.382 0.25513
% use:category                  2  0.246   0.123   5.191 0.00688 **
% prof:continuum                1  0.000   0.000   0.001 0.97195
% use:continuum                 1  0.002   0.002   0.103 0.74889
% prof:use:category             2  0.130   0.065   2.736 0.06890 .
% prof:use:continuum            1  0.003   0.003   0.112 0.73833
% prof:category:continuum       2  0.002   0.001   0.038 0.96241
% use:category:continuum        2  0.035   0.017   0.736 0.48139
% prof:use:category:continuum   2  0.004   0.002   0.081 0.92259
% Residuals                   120  2.848   0.024
%
A 2-way interaction for category slopes between language use and category was found (\fvalue{2,120}{5.19}{<0.01}), with participants who reported using their \gls*{L2} more frequently across contexts appearing to have steeper slopes for the oxytone category only, while no change was shown for the other two categories.

\marginpar{Marginally significant interactions}%
Other effects were found to be just outside the threshold of significance.
These included a main effect of use (\fvalue{2,73}{3.85}{=0.054}) and an interaction between proficiency and category (\fvalue{2,73}{2.84}{=0.065}) for boundary positions; and a three-way interaction between category, language use and language proficiency (\fvalue{2,120}{2.74}{=0.069}) on category slopes.

\section{Discussion}
\label{sec:2_spanish_discussion}

\subsection{Sensitivity to duration}

\marginpar{Similar sensitivity to duration for both language groups}%
As predicted in \cref{sec:2_acoustics_discussion_spanish}, results from this study show that \gls*{js} participants are not only sensitive to duration changes for the perception of non-native lexical prominence, but that, at least in the first contrast, this sensitivity is similar to that of native speakers.
When listening to stimuli from the first contrast, participants from both the native and the non-native groups showed significantly different boundary positions and slopes depending on whether the stimuli had the duration of a proparoxytone or a paroxytone word.
But language group was not a significant predictor of responses either on its own or in combination with other factors.
This degree of similarity is surprising even in the light of the native-like performance that \gls*{js} participants showed for isolated words in \cref{chp:1_spanish}, since these students had only ever studied Spanish in \gls*{fla} environments.

\marginpar{The second contrast}%
Responses for the second contrast show a greater difference between native and non-native participants, with non-native speakers having different boundary positions than natives per category.
But notably absent as a significant effect is the interaction between language group and continuum that would denote a difference in the sensitivity for duration as a cue.

Based on the relatively low performance exhibited by \gls*{js} participants with the prominence of oxytone words (even in isolation), responses for the second contrast \hyperref[sec:2_acoustics_discussion_spanish]{were predicted} to be lower in the second contrast, and lower in particular for oxytone words.
This was the case, as is reflected in the lower maximum number of oxytone responses for both continua in \cref{fig:2_spanish_results_mid-lst}, and the significantly lower slopes for non-native oxytone responses in continuum A$_2$ in \cref{fig:2_spanish_slopes_continuum_group_category_mid-lst}.

\marginpar{Effects of conflicting cues}%
The lower non-native performance with word-final prominence gets confounded with the effects of conflicting acoustic cues, which is likely what explains the effect shown in particular for continuum A$_2$.
As explained in \cref{sec:2_acoustics_discussion_spanish}, the conflict between the information encoded in the duration and the \fzero\ of the stimuli would be strongest at the beginning of the B continua and the end of the A continua, which would predict that a population that was particularly sensitive to the interaction between those cues would show poorer performances in those stimuli.
Interestingly, the results not only show precisely this effect (which is much stronger for the B$_2$ continuum), but they also show that it affects both language groups in remarkably similar manners.

\marginpar{Cue-weighting}%
Still, responses for the second contrast in particular show some differences between language groups: \gls*{sj} listeners tend to place their boundaries later, which is an indication that, while both groups are sensitive to duration, the evaluation that \gls*{sj} learners make of that cue is subject to transfer effects from Japanese.
But the effects are small, and they only became apparent for the second contrast.
This makes it possible that there is a connection between these transfer effects and their additional difficulty with prominence that occurs late in the word.
Further work is needed to fully explore this possible connection.

\subsection{Language proficiency}

Unlike this study, the study reported in \cref{chp:1_spanish} did not show any effects of language proficiency.
This was originally explained as an effect of the poverty of the \gls*{fla} learning environment and the lack of priority in prosodic training.
Results from this study, however, question those conclusions.

Although participants for both studies come from different pools, they represent the same populations: they have comparable age ranges and distributions and they reported similar levels of language proficiency.
There is a possibility that their learning environments were different, with most of the participants for this study coming from Sophia University, a university in which no recruitment was done for the previous study.
But both groups of participants were \gls*{fla} students, and in an exit interview that took place after the experiment was concluded, participants from this study did not report going through any special prosodic training.

It is much more likely that the specifics of this design allow for more granularity in the detection of the effects of \gls*{L2} proficiency, and that the learning conditions that the participants were under made the effect too small to be detected before this.
Although this dissertation did not perform any longitudinal analyses, the performance of participants with different overall measures of proficiency (including their reports of proficiency itself and language use) can be understood as snapshots of the performance at different stages in the development of language proficiency.
Understood in this sense --- and with the caveat that participants represented a relatively narrow scope of proficiency measures --- results can be interpreted as a sign that improvement through instruction is possible.
Considering the learning environments of these participants, this means that a more targeted training could have a much greater impact, which should serve as a considerable motivation for prioritising prosodic training in the future.

\fleuron
