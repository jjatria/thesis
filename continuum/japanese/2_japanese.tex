\chapter{\texorpdfstring%
  {Duration effects on the perception of Japanese as \glstext*{L2}}%
  {Duration effects on the perception of Japanese as L2}%
}
\chaptermark{Japanese prominence: effects of duration}
\label{chp:2_japanese}

\renewcommand{\topfraction}{0.9}  % max fraction of floats at top
\renewcommand{\bottomfraction}{0.8}

\blankfootnote{Part of the results presented in this chapter were presented at the \nth{18} meeting of the International Congress of Phonetic Sciences (\textsc{ICPhS}) in Glasgow, Scotland, in the summer of 2015.
Digital materials related to this study can be found in the study's repository at \url{http://www.pinguinorodriguez.cl/research/phd/secondary-cues/japanese}.}

The \hyperref[chp:2_spanish]{previous chapter} reported the results of a study on the effects of duration as a secondary cue for the perception of Spanish lexical prominence.
This chapter presents the results of the application of the same methodology (described in \cref{chp:2_acoustics}) to the case of \gls*{sj}.

\section{Design}
\label{sec:2_japanese_design}

\marginpar{Why duration in Japanese?}%
Like in the study reported in \hyperref[chp:2_spanish]{the previous chapter}, the main motivation for this study was also to examine the effect that secondary acoustic cues had on the perception of non-native suprasegmental categories.
However, while the procedure in both cases was kept to maintain maximum comparability between both studies (see \cref{sec:bidirectional_approach}), the question that was of most interest in the case of Japanese as an \gls*{L2} was different.

Both this study and that of \cref{chp:2_spanish} focus on the effects of duration in particular.
This decision was an obvious one for Spanish, since duration is the main secondary cue for the perception of stress (see \cref{sec:spanish_cues}), but as explained in \cref{sec:japanese_accent_types}, duration does not play any role whatsoever in the encoding or the perception of Japanese lexical prominence.

Indeed, \cref{fig:2_japanese_duration_ratios_per_accent} (on page \pageref{fig:2_japanese_duration_ratios_per_accent}) shows that, while different syllables will have different durations in Japanese, there is no large difference in the distribution of syllable durations across words with different accent types.
This contrasts strongly with the case of Spanish, illustrated in \cref{fig:2_spanish_duration_ratios_per_accent}, which shows a strong interaction between the lengthening of final syllables and that of prominent syllables.
However, results from \cref{chp:1_japanese} suggested that \gls*{sj} listeners might be sensitive to non-systematic variation in the cues that Japanese does not consider relevant, like duration and intensity, of which duration was particularly interesting (see \cref{sec:2_acoustics_materials}).

The chosen methodology makes it possible to examine the side effects of whatever small changes in duration may exist, while at the same time illustrating the degree of category development in \gls*{L2} listeners, which in this case was a much more intriguing question.
This because \cref{chp:1_japanese} showed extremely low performances for \gls*{sj} participants in the identification of a contrast that, from an auditory perspective, should not have been as difficult: that between accented and unaccented words.

\marginpar{Research variables}%
Like in \cref{chp:2_spanish}, the sensitivity to duration as a secondary cue was operationalised as the magnitude of the shift in the response functions across continua (and therefore of the categorical boundaries of those functions); while differences in the weighing would be made evident by differences in the shift across language groups.
The degree of category development would be correlated with the steepness of those response functions, measured by the rate of change of that curve, or its slope.
\marginpar{Linguistic questionnaire}%
Additionally, participants in this study filled in the same linguistic questionnaire as in the previous chapter, providing information about their language proficiency.
This would make it possible to measure the effects of language training on their perception.

\subsection[Participants]{\texorpdfstring%
  {Participants%
  \footnote{Special thanks to Rosa Catalán, Imran Hossain, and Karina Cerda, and to professor Domingo Román in Santiago; and professors Arai Takayuki, Kimiyo Nishimura, and Yasuaki Shinohara in \Tokyo\ for assistance with participant recruitment and data collection.}}%
  {Participants}
}
\label{sec:2_japanese_participants}

\marginpar{Chilean participants: \gls*{sj}}%
A total of 34 (26 female and 8 male) Chilean participants between the ages of 14 and 33 (\mean{22.3}{4.1}) were recruited from the student body enrolled in the undergraduate Japanese translation program at Universidad de Santiago, in Santiago.
\Cref{fig:2_japanese_proficiency} shows the distribution of participants according to the three self-reported measures of second language proficiency: length of studies, overall proficiency, and frequency of use.

\begin{figure} % fig:2_japanese_proficiency
  \centering
  \input{\FIGURES/2_japanese_proficiency}%
  \caption[Self-reported measures of proficiency of \gls*{sj} participants]{%
    Self-reported measures of \gls*{L2} proficiency of \gls*{sj} participants.
    The horizontal position of each circle marks the mean score reported by each participant to the three scales.
    In the case of proficiency and use, participants were given a 5-point scale, while they were free to enter how many years they had studied for.
    The area of each circle reflects the number of participants for that particular score.
  }
  \label{fig:2_japanese_proficiency}
\end{figure}

\marginpar{Japanese participants}%
Japanese controls were recruited in \Tokyo\ largely from the student body of the \Tokyo\ University of Foreign Studies and Sophia University, in \Tokyo.
A total of 35 (18 female and 17 male) participants from the \Kanto\ region between 18 and 49 years old (\mean{23.6}{6.8}) participated in the test.

Before the test, participants were asked to fill a linguistic background survey providing broad information about their \gls*{L2} proficiency and use.
Reports from \gls*{sj} participants were very similar to those obtained from \gls*{js} participants in the previous test: the large majority of participants claimed to be in a beginner-to-intermediate level (\mean{2.06}{0.77}), and to use their \gls*{L2} just under once per week (\mean{2.81}{0.86}).

\subsection{The test procedure}
\label{sec:2_japanese_test_procedure}

The test followed exactly the same procedure as the one described in detail in \cref{sec:2_spanish_test_procedure}: participants took the test locally in Chile and Japan wearing headphones provided by the examiners.
Testing session similarly lasted \textasciitilde45 minutes, including regular breaks.
Participants listened to each of the 40 tokens, once per test block, for a total of 240 stimuli ($40 \times 6$), half of them with each speaker, and were tasked with identifying the word they had heard from a list.

Like in \cref{chp:1_japanese}, all instructions and button labels in the test were presented in Japanese, using the standard Japanese orthography in \emph{kanji} and \emph{hiragana} script.
Since Japanese orthography does not mark the position of the accent, unlike the Spanish orthography, the accented syllable was marked in red.
This included the items in the practice rounds, during which any questions they had on the procedure could be clarified by the examiners.

\section{Results}
\label{sec:2_japanese_results}

Analysis of the responses followed the exact same procedure as used in \cref{chp:2_spanish}.

\Cref{fig:2_japanese_results_fst-mid,fig:2_japanese_results_mid-lst} provide an overview of the obtained responses by showing the mean percentage of responses per category for each step in the continua, separated horizontally by continua (\ie~by the reference point used for syllable duration) and vertically by language group.

\subsection[First contrast]{\texorpdfstring%
  {First contrast: initial- \versus\ final-accented}%
  {First contrast}%
}

Native responses for stimuli from the first contrast (shown at the top of \cref{fig:2_japanese_results_fst-mid}) show the expected pattern of responses, with very high percentages of responses for the categories involved in the contrast (\ie~first- and final-accented) in the appropriate extremes of the continua, and extremely low numbers of responses throughout for the third category (\ie~unaccented).
Steep slopes also show that the responses of native participants are categorical in nature, which is evidence that the possible artifacts introduced by the synthesis did not have a large impact on their responses, as suggested by the initial evaluation of the stimuli.

Non-native responses, in the same figure, show a similar pattern of categorical perception, albeit vertically compressed at the extremes.
A side effect of this compression is an increase in the number of unaccented responses, but even though they reached an average of 25\percent\ of the total at the rightmost step of both continua, they still maintained a low number throughout.

\begin{figure}
  \centering
  \input{\FIGURES/2_japanese_results_fst-mid}%
  \caption[Averaged response functions for the first contrast]{%
    Percentage of responses per category at each step in the continua for the first contrast across all participants and speakers.
    Stimuli with initial- and final-accented durations are shown in the left and right columns respectively, while rows separate responses between language groups.
    The \showcolor{chance} line marks the chance level.
    Compare with Spanish results in \cref{fig:2_spanish_results_fst-mid}, on page \pageref{fig:2_spanish_results_fst-mid}.
  }
  \label{fig:2_japanese_results_fst-mid}
\end{figure}

\subsubsection{Category boundaries}

%                           Df Sum Sq Mean Sq F value   Pr(>F)
% continuum                  1   1.81    1.81   1.632 0.202870
% group                      1  11.50   11.50  10.366   0.0015 **
% category                   1 104.55  104.55  94.266  < 2e-16 ***
% continuum:group            1   2.73    2.73   2.461 0.118286
% continuum:category         1   0.04    0.04   0.035 0.851606
% group:category             1  21.62   21.62  19.490 1.65e-05 ***
% continuum:group:category   1   0.08    0.08   0.075 0.785036
% Residuals                201 222.93    1.11
%
% group                      1  13.27   13.27  11.966 0.000661 *** | With the N outlier included
% category                   2 114.20   57.10  51.480  < 2e-16 *** | With the N outlier included

A main effect of language group was found for the position of category boundaries (\fvalue{1,201}{10.37}{<0.01}), with those of native listeners (\mean{6.84}{0.83}) occurring slightly later than those of \gls*{sj} participants (\mean{6.34}{1.61}).
A difference between the boundary position of the categories in the contrast was also found (\fvalue{1,201}{94.27}{<0.001}).
In a 2-way contrast, boundaries understood as the point at which responses cross the 50\percent\ mark should not differ between categories.
But in this case, the presence of a significant number of unaccented responses made the boundaries differ, such that the boundary of first-accented words (\mean{5.86}{1.27}) occurred earlier than those of final-accented words (\mean{7.29}{0.94}).

This is not entirely attributable to the responses of \gls*{sj} listeners, however, as attested by the presence of a significant 2-way interaction between language group and accent category (\fvalue{1,201}{57.1}{<0.001}), shown in \cref{fig:2_japanese_boundaries_group_category_fst-mid}.
As shown in the figure, while the difference between the position of the boundary for non-native listeners ($1.99$) is much greater than that of native listeners ($0.69$), this latter difference is also significant (\ttest{85.56}{-4.83}{<0.001}).

\begin{figure} % fig:2_japanese_boundaries_group_category_fst-mid
  \centering
  \input{\FIGURES/2_japanese_boundaries_group_category_fst-mid}%
  \caption[Category boundaries per group and accent category]{%
    Distribution of category boundary positions per group and accent category for the first Japanese contrast.
    Numbers above each box indicate the corresponding $n$, after discounting participants for which boundary positions could not be estimated.
  }
  \label{fig:2_japanese_boundaries_group_category_fst-mid}
\end{figure}

\subsubsection{Category slopes}

%                           Df Sum Sq Mean Sq F value Pr(>F)
% continuum                  1   0.00   0.001   0.022  0.883
% group                      1   0.01   0.010   0.221  0.638
% category                   2  59.34  29.669 688.658 <2e-16 ***
% continuum:group            1   0.00   0.001   0.021  0.884
% continuum:category         2   0.05   0.023   0.533  0.588
% group:category             2  13.37   6.686 155.186 <2e-16 ***
% continuum:group:category   2   0.03   0.016   0.366  0.694
% Residuals                330  14.22   0.043

The fact that both groups had particularly low responses for the category that was absent from the contrast was reflected in a significant difference in the slopes of those categories (\fvalue{1,330}{688.66}{<0.001}), with that of the unaccented category being very close to zero (\mean{0.09}{0.08}), and the slopes of the other categories being at more or less equal distances from it (\mean{-0.53}{0.36}; \mean{0.48}{0.33}, for initial- and final-accented words respectively).

This difference in slopes for different categories varied across language groups, as shown by a significant 2-way interaction between these two factors (\fvalue{1,330}{155.19}{<0.001}).
This interaction, shown in \cref{fig:2_japanese_slopes_group_category_fst-mid}, reveals that native speakers gave more categorical responses, with the difference between mean slopes for the initial- and final-accented categories being much greater for native listeners ($1.6$) than for \gls*{sj} ($0.62$).

\begin{figure} % fig:2_japanese_slopes_group_category_fst-mid
  \centering
  \input{\FIGURES/2_japanese_slopes_group_category_fst-mid}%
  \caption[Category boundaries per group and accent category]{%
    Distribution of category slopes per group and accent category for the first Japanese contrast.
    Numbers above each box indicate the corresponding $n$.
  }
  \label{fig:2_japanese_slopes_group_category_fst-mid}
\end{figure}

\subsubsection{\texorpdfstring%
  {\Gls*{L2} proficiency}%
  {L2 proficiency}%
}

% Boundaries
%
%                             Df Sum Sq Mean Sq F value   Pr(>F)
% prof                         1   0.15    0.15   0.071    0.791
% use                          1   1.46    1.46   0.706    0.404
% prof:use                     1   0.93    0.93   0.451    0.504
% prof:category                1   0.24    0.24   0.116    0.734
% use:category                 1   0.14    0.14   0.067    0.796
% prof:continuum               1   0.26    0.26   0.128    0.722
% use:continuum                1   0.56    0.56   0.273    0.603
% prof:use:category            1   0.16    0.16   0.080    0.778
% prof:use:continuum           1   0.34    0.34   0.165    0.686
% prof:category:continuum      1   0.48    0.48   0.230    0.633
% use:category:continuum       1   0.09    0.09   0.046    0.831
% prof:use:category:continuum  1   1.13    1.13   0.547    0.462
% Residuals                   73 150.77    2.07

% Slopes
%
%                              Df Sum Sq Mean Sq F value   Pr(>F)
% prof                          1  0.000   0.000   0.031   0.8597
% use                           1  0.002   0.002   0.140   0.7091
% prof:use                      1  0.005   0.005   0.302   0.5837
% prof:category                 2  0.025   0.012   0.782   0.4598
% use:category                  2  0.348   0.174  10.936 4.03e-05 ***
% prof:continuum                1  0.002   0.002   0.125   0.7241
% use:continuum                 1  0.003   0.003   0.167   0.6836
% prof:use:category             2  0.121   0.061   3.802   0.0248 *
% prof:use:continuum            1  0.001   0.001   0.059   0.8092
% prof:category:continuum       2  0.004   0.002   0.120   0.8869
% use:category:continuum        2  0.004   0.002   0.124   0.8839
% prof:use:category:continuum   2  0.018   0.009   0.563   0.5707
% Residuals                   132  2.101   0.016

No effects of \gls*{L2} proficiency were found for the position of category boundaries, but there was a significant interaction between language use and accent category for category slopes (\fvalue{2,132}{10.94}{<0.001}) such that participants who reported using their \gls*{L2} less often tended to have flatter response functions than those that reported relatively high degrees of language use.
A significant 3-way interaction between these two factors and the aggregate measure of \gls*{L2} proficiency (\fvalue{2,132}{3.8}{<0.05}) was also found.

\begin{figure} % fig:2_japanese_slopes_use_fst-mid
  \centering
  \input{\FIGURES/2_japanese_slopes_use_fst-mid}%
  \caption[Interaction between category slopes and \gls*{L2} use]{%
    Interaction between slopes per category for the first contrast and self-reported language use of \gls*{sj} participants.
  }
  \label{fig:2_japanese_slopes_use_fst-mid}
\end{figure}

\subsection[Second contrast]{\texorpdfstring%
  {Second contrast: final-accented \versus\ unaccented}%
  {Second contrast}%
}

\Cref{fig:2_japanese_results_mid-lst}, which shows the results for the second contrast, presents a strikingly different scenario to that shown in the previous section.

Responses for the native speakers show the same response patterns, with if anything fewer responses for the absent category (in this case, that of initial-accented words), strong evidence that in this contrast too the synthesis did not introduce any undesired effects.
But responses for the non-native listeners show a dramatic difference, with response functions that are almost entirely flat and hover around the level of chance responses.
Interestingly, despite this extremely poor performance the averages of the response functions for the relevant categories do show a tendency in the appropriate direction, such that unaccented responses are marginally higher at the unaccented end of the continua, and the same for final-accented responses.
As will be seen \hyperref[sec:2_japanese_slopes_non-native_trend]{below}, this difference was significant.

\begin{figure} % fig:2_japanese_results_mid-lst
  \centering
  \input{\FIGURES/2_japanese_results_mid-lst}%
  \caption[Averaged response functions for the second contrast]{%
    Percentage of responses per category at each step in the continua for the first contrast across all participants and speakers.
    Stimuli with final-accented and unaccented durations are shown in the left and right columns respectively, while rows separate responses between language groups.
    The \showcolor{chance} line marks the chance level.
    Compare with Spanish results in \cref{fig:2_spanish_results_mid-lst}, on page \pageref{fig:2_spanish_results_mid-lst}.
  }
  \label{fig:2_japanese_results_mid-lst}
\end{figure}

\subsubsection{Category boundaries}

%                           Df Sum Sq Mean Sq F value   Pr(>F)
% continuum                  1    1.9    1.93   0.934 0.335328
% group                      1   23.6   23.62  11.453 0.000893 ***
% category                   2   67.9   33.97  16.467 3.04e-07 ***
% continuum:group            1    0.3    0.27   0.132 0.717282
% continuum:category         2    4.5    2.25   1.089 0.338993
% group:category             1   34.6   34.57  16.758 6.65e-05 ***
% continuum:group:category   1    1.3    1.26   0.613 0.434976
% Residuals                164  338.3    2.06

Results shown in \cref{fig:2_japanese_results_mid-lst} show that non-native responses for this contrast were almost entirely flat.
However, there was a great deal of individual variation, which caused the responses for a large number of participants to cross the 50\percent\ threshold.
This point should be kept in mind while considering the effects described in this section.

A main effect of language group was found for category boundaries (\fvalue{1,164}{11.45}{<0.001}), such that boundaries for native listeners (\mean{4.84}{0.91}) occurred earlier than those of \gls*{sj} listeners (\mean{5.59}{2.14}).
Likewise, a main effect of category was also found to be significant (\fvalue{1,164}{16.47}{<0.001}), but largely as a result of the variation of non-native responses.
In both cases, the extremely large standard deviation for the non-native responses is explained by the issue described above.

More revealing is a significant 2-way interaction between language group and category (\fvalue{1,164}{16.76}{<0.001}), shown in \cref{fig:2_japanese_boundaries_group_category_mid-lst}, which illustrates the extent of the difference between native and non-native responses.
The low number of native initial-accented responses (for which no boundaries were registered) mean that the average boundary positions of the remaining two categories show almost no difference ($0.31$), a fact that is certainly not the case for non-native listeners.
Interestingly, the position of the detected boundaries for non-native final-accented responses aligns closely with that of the native listeners, and they show significantly less variance than the rest of the categories.

\begin{figure} % fig:2_japanese_boundaries_group_category_mid-lst
  \centering
  \input{\FIGURES/2_japanese_boundaries_group_category_mid-lst}%
  \caption[Category boundaries per group and accent category]{%
    Distribution of category boundary positions per group and accent category for the second Japanese contrast.
    Numbers above each box indicate the corresponding $n$, after discounting participants for which boundary positions could not be estimated.
  }
  \label{fig:2_japanese_boundaries_group_category_mid-lst}
\end{figure}

\subsubsection{Category slopes}
\label{sec:2_japanese_slopes_mid-lst}

%                           Df Sum Sq Mean Sq F value   Pr(>F)
% continuum                  1   0.02   0.023   0.099    0.753
% group                      1   0.12   0.123   0.523    0.470
% category                   2  17.10   8.549  36.427 5.07e-15 ***
% continuum:group            1   0.13   0.133   0.568    0.452
% continuum:category         2   0.05   0.023   0.098    0.907
% group:category             2  10.45   5.225  22.263 8.52e-10 ***
% continuum:group:category   2   0.30   0.149   0.635    0.530
% Residuals                330  77.45   0.235

A main effect of accent category was also found to be significant for category slopes (\fvalue{2,330}{36.43}{<0.001}), with overall values for the slope of initial-accented responses being much closer to zero (\mean{-0.05}{0.79}) than those of the other two categories (\mean{-0.27}{0.27}; \mean{0.27}{0.29}, for final-accented and unaccented respectively).
A significant 2-way interaction between language group and accentual category (\fvalue{2,330}{22.26}{<0.001}), shown in \cref{fig:2_japanese_slopes_group_category_mid-lst}, presents a better picture of these results by breaking up responses for native and non-native listeners.
\anchor{sec:2_japanese_slopes_non-native_trend}%
This shows that category slopes for \gls*{sj} are all very close to zero, while those for native speakers present a much wider spread.
As stated before, however, it bears pointing out that the slopes of the relevant categories for non-native speakers point in the right direction.
A pairwise Bonferroni-corrected t-test between the non-native responses per category was used to check whether these slope values were significantly different from each other, and results showed this to be the case for all of them (even if the difference itself is small).

\begin{figure} % fig:2_japanese_slopes_group_category_mid-lst
  \centering
  \input{\FIGURES/2_japanese_slopes_group_category_mid-lst}%
  \caption[Category boundaries per group and accent category]{%
    Distribution of category slopes per group and accent category for the second Japanese contrast.
    Numbers above each box indicate the corresponding $n$.
  }
  \label{fig:2_japanese_slopes_group_category_mid-lst}
\end{figure}

\subsubsection{\texorpdfstring%
  {\Gls*{L2} proficiency}%
  {L2 proficiency}%
}

% Boundaries
%
%                             Df Sum Sq Mean Sq F value   Pr(>F)
% prof                         1   3.24    3.24   0.824 0.369931
% use                          1   3.39    3.39   0.864 0.358569
% prof:use                     1   1.09    1.09   0.278 0.601050
% prof:category                2  20.99   10.49   2.672 0.082409 .
% use:category                 2  17.19    8.60   2.189 0.126309
% prof:continuum               1   0.37    0.37   0.095 0.759782
% use:continuum                1   0.02    0.02   0.006 0.937684
% prof:use:category            2   4.88    2.44   0.621 0.543038
% prof:use:continuum           1   0.00    0.00   0.000 0.995509
% prof:category:continuum      2   1.11    0.55   0.141 0.868912
% use:category:continuum       2   4.37    2.19   0.557 0.577784
% prof:use:category:continuum  1   1.26    1.26   0.322 0.573815
% Residuals                   37 145.29    3.93

% Slopes
%
%                              Df Sum Sq Mean Sq F value Pr(>F)
% prof                          1 0.0001  0.0001   0.009 0.9243
% use                           1 0.0027  0.0027   0.285 0.5946
% prof:use                      1 0.0003  0.0003   0.031 0.8608
% prof:category                 2 0.0145  0.0072   0.766 0.4670
% use:category                  2 0.0244  0.0122   1.291 0.2785
% prof:continuum                1 0.0039  0.0039   0.412 0.5221
% use:continuum                 1 0.0052  0.0052   0.553 0.4584
% prof:use:category             2 0.1424  0.0712   7.530 0.0008 ***
% prof:use:continuum            1 0.0067  0.0067   0.704 0.4028
% prof:category:continuum       2 0.0044  0.0022   0.233 0.7928
% use:category:continuum        2 0.0017  0.0008   0.088 0.9160
% prof:use:category:continuum   2 0.0139  0.0070   0.736 0.4808
% Residuals                   132 1.2484  0.0095

As for \gls*{L2} proficiency, the only significant result was a high-level 3-way interaction between category, language use use and language proficiency which was found for category slopes (\fvalue{2.132}{7.5}{<0.001}).

\section{Discussion}
\label{sec:2_japanese_discussion}

\subsection{Sensitivity to duration}

Results from \cref{chp:1_japanese} suggested that the perception of \gls*{sj} participants might have been impaired in part by an excessive sensitivity to random variation in acoustic cues like duration and intensity that were not being controlled for by native speakers of Japanese but were meaningful in Spanish.

The small differences in the distribution of syllable durations in the original stimuli meant that there was a real possibility that no difference in the responses of \gls*{sj} would show, despite the fact that they are known to have a strong sensitivity to duration as a cue for suprasegmental categories.
The results described above showed that this was indeed the case, with no difference in the responses across continua being significant for either slopes or boundaries, either on its own or in combination with other factors.

\begin{figure} % fig:2_japanese_boundaries_continua
  \centering
  \input{\FIGURES/2_japanese_boundaries_continua}%
  \caption[Effects of duration on the position of category boundaries]{%
    Effects of duration on the position of category boundaries per language group (vertically), and contrast (horizontally).
    Numbers above each box indicate the corresponding $n$, after discounting participants with no estimated boundary positions.
  }
  \label{fig:2_japanese_boundaries_continua}
\end{figure}

\marginpar{Marginal effects of duration}%
However, particularly considering how little difference there was in the stimuli to begin with, results for the relevant interaction between language group and continuum on the position of categorical boundaries were surprisingly close to significance (\fvalue{1,201}{2.461}{=0.118}).
\Cref{fig:2_japanese_boundaries_continua} shows the data behind this interaction, and shows how similar the responses are for all groups.
It also shows that the group that shows the most difference is precisely that of non-native responses for the first contrast, with boundaries in the A continuum (\mean{6.14}{1.66}) occurring ever so slightly \emph{earlier} than those in the B continuum (\mean{6.55}{1.55}).

To be clear, this is most certainly not evidence of an effect, and it should be noted that the direction of the possible boundary shift goes in the opposite direction that would be expected.
And any number of acoustic properties might be involved in such a small effect, a possibility which should be taken particularly seriously considering the stimuli were synthesised using a method (\tandemstraight) that prioritises sound quality over minute feature control (see \cref{sec:2_acoustics_manipulation}).
But both groups of participants were presented with the same stimuli, and responses from native listeners strongly support the notion that, if any artifacts were introduced during manipulation, they were well below the perceptual threshold that is relevant for Japanese at least.
And even if other sources of variation managed to go uncontrolled, duration should still be the one that varied most widely, since all other features in the synthesis were fixed to common values while duration was intentionally stretched (even if slightly).

\subsection{Degree of category development}

Results showed a clear difference between the responses for the first and the second contrast, with non-native participants showing a progressively compressed response space between both.
However, within this space participants did show evidence of some degree of categorical development (in the first contrast) or at the very least a sensitivity to the relevant acoustic cues (in the second).
In the first of these, \gls*{sj} participants showed the characteristic pattern of categorical responses, with response functions that reached a plateau at the ends of the continua.
And while the degree of the compression in the last contrast makes it hard to say this with certainty, there is evidence that even here participants show the potential to identify the relevant categories.

This is supported by the slope values measured for the non-native responses.
Even in the second contrast, category slopes were statistically different for all category pairs, and described responses that increased as the stimuli approached the appropriate end of the continuum (\ie~the slopes had the correct mathematical sign).

\marginpar{Compression of response space}%
The compression in the response space of non-native participants accurately mirrors the responses reported in \cref{sec:1_japanese_results}, shown in more detail in \cref{fig:1_japanese_accent_effect_trios}.
Those results showed that the mean correct identification of the position of Japanese prominent syllables by \gls*{sj} across sentence contexts was of approximately 75\percent\ for initial-accented words, 50\percent\ for final-accented words, and 33\percent\ for unaccented words, which are very close to the maximum numbers obtained at the extremes of the continua in this study.
The match between the results of these two studies, which used different methodologies on different participant pools, lends additional support to those results.

But it also highlights one possible limitation of the design, in that this study also fundamentally relied in categorical identification, which had already been shown to be problematic for \gls*{sj} participants.
More concretely, the methodology's definition of the threshold for categorical boundaries and its dependence on that notion, particularly in light of the large number of \gls*{sj} participants who never crossed the threshold for the second contrast, means that perhaps an altogether different method should be used to explore in more detail the behaviour of the non-native participants.

In particular, the results reported by \cref{chp:1_japanese} for the /momo/ contrast between initial-accented and unaccented words (see \cref{fig:1_japanese_accent_effect_pair}) suggest that approaching the topic from the perspective of auditory discrimination might yield more information regarding the difficulties faced by \gls*{sj} participants.

Despite those problems, the additional granularity of the approach used in this study (discussed in the previous chapters) revealed new information on the degree of categorical development of \gls*{sj}, and provided evidence that at the very least does not disprove the explanations given to the results of \cref{chp:1_japanese}.

\subsection{Language proficiency}

Like in the study reported in \cref{chp:2_spanish}, this study showed the effects of \gls*{L2} proficiency where the study in \cref{chp:1_japanese} was not able to do so.
Results from this study suggested a correlation in particular between use of the \gls*{L2} and the degree of development of accentual categories.
The implications in this case, like in \cref{chp:2_spanish} should serve as additional motivation for a renewed effort in prosodic training, and in the search for training methods that can provide lasting benefits in the perception of suprasegmentals as well.

\fleuron
