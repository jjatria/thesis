\documentclass[11pt]{article}

\usepackage{xparse}
\usepackage{xcolor}
\definecolor{refcolor}{HTML}{0F4DA8}
\definecolor{linkcolor}{HTML}{840084}

\usepackage[
  colorlinks,
  citecolor=refcolor,
  linkcolor=linkcolor,
  urlcolor=linkcolor,
  linktocpage=true
]{hyperref}

\hyphenation{
  Ja-pa-nese
}

\frenchspacing
\usepackage{microtype}
\renewcommand\baselinestretch{1.5}

\usepackage[british,abbreviations]{foreign}
\newcommand{\Tobi}{\textsc{T}o\textsc{bi}}
\newcommand{\tobi}{\textsc{t}o\textsc{bi}}
\newcommand{\fzero}{\textsc{f}$_0$}
\let\unicodeL\L
\DeclareDocumentCommand\L{ g }{%
  \IfValueTF{#1}{\textsc{l}#1}{\unicodeL}%
}

\usepackage{fontspec}
\setmainfont{Linux Libertine O}[
  Numbers={OldStyle,Proportional},
  Mapping=tex-text,
]
\setsansfont{Linux Biolinum O}[
  Mapping=tex-text,
]
\setmonofont{Inconsolata}[
  Mapping=tex-text,
]
\newfontfamily{\monospaced}{Linux Libertine O}[
  Numbers={Monospaced, OldStyle},
  Mapping=tex-text,
]

% Typeset lists
\usepackage{enumitem}
\setlist[enumerate]{
  leftmargin=0pt,
  rightmargin=0pt,
%   rightmargin=\parindent,
  nosep,
  label=\arabic*,
}
\setlist[enumerate,2]{
  rightmargin=0pt,
  leftmargin=0pt,
  label*=.\arabic*,
}
\setlist[enumerate,3]{
  rightmargin=0pt,
  leftmargin=0pt,
  label*=.\arabic*,
}

\usepackage{setspace}
\usepackage[
  rightmargin = 0pt,
%   vskip       = 0pt,
  leftmargin  = 1cm,
  font+       ={smaller}
]{quoting}
\renewcommand*{\quotingfont}{\setstretch{1.5}}

\usepackage{scrextend}
\deffootnote{0em}{1.6em}{\thefootnotemark\enskip}
\let\svthefootnote\thefootnote
\newcommand\blankfootnote[1]{%
  \let\thefootnote\relax\footnotetext{#1}%
  \let\thefootnote\svthefootnote%
}

\let\oldmarginpar\marginpar
\newcommand{\marginformat}[1]{%
  \setstretch{1}\footnotesize\sffamily#1}
\renewcommand{\marginpar}[1]{%
  \leavevmode\oldmarginpar[\marginformat{\raggedleft#1}]{\marginformat{\raggedright#1}}}

\usepackage{geometry}
\geometry{
  a4paper,                           % 210 x 297 mm
  inner          = 0.111\paperwidth, % 23.333mm + bindingoffset?
  top            = 0.111\paperwidth, % 23.333mm
  bottom         = 0.222\paperwidth, % 46.666mm
  outer          = 0.222\paperwidth, % 46.666mm
  marginparsep   = 5mm,
  marginparwidth = 7em,
  footnotesep    = 5mm,
  headheight     = 14pt,
  % showframe
}
% Is this necessary?
\geometry{
  bindingoffset  = 0.111\paperwidth, % 23.333mm
}

\title{Correction report}
\author{José Joaquín Atria}
\date{March 18, 2016}

\usepackage{pgffor}
\newcommand{\difflink}[1]{\marginpar{
  \foreach \diff in {#1} {
    \href{https://gitlab.com/jjatria/thesis/commit/\diff}{\diff}\\
  }
}}

\usepackage{cleveref}

\begin{document}

\maketitle

\blankfootnote{References on the margin link to the specific changes made to the document.}%

\begin{enumerate}
\item \label{litreview}
Introduction:
The state of the art presented in chapter 1 shows a good critical understanding of recent research in the field.
However, some more detailed consideration of earlier work would have been appropriate, particularly with respect to the experimental studies on lexical stress in Spanish.
Also a more exhaustive review of work on the perception of lexical stress in \L{2} in other languages would be useful as it would provide a more comprehensive picture of the research domain.

\begin{quoting}
\difflink{0699eaf,bc8267f,c73fb49}%
The section in the introduction detailing the acoustic cues of the Spanish prominence has been expanded to include a more detailed revision of the literature and the discussion within it, and a more critical appraisal of the studies mentioned.
\difflink{0845c5f}%
New and more recent references on difficulties faced by Japanese learners of Spanish were also added.

\difflink{8609d4b,0869a46}%
I also added more detailed coverage of the rate of occurrence and factors involved in peak displacement, as well as references to more studies on Chilean Spanish in particular, including a brief discussion of evidence that might support a difference in the distribution of peak displacement for the Chilean variety.

\difflink{3ec0b5f}%
In particular, a review of literature regarding stress deafness and \L{2} perception of prominence was also added to provide a better (and more critical) revision of the relevant literature on \L{2} perception of suprasegmentals.
\end{quoting}

\item \label{questionnaires}
Participants in experimental studies:
The choice of self-assessment questionnaires as a means for evaluating spoken language proficiency should be justified in more detail.
The topic is addressed in section 1.4.2 and in appendix \textsc{f}.
However, as indicated above, the candidate did extra background work (not reported).
There are more robust alternatives that might have been used.
Reference to two possibilities published by Li and associates are given below.
The candidate should discuss these and other alternatives and justify the use of his non-standard self-reported proficiency questionnaire.

\begin{quoting}
\difflink{f751d2b,1dbefaa}%
Linguistic questionnaires have been used extensively in \L{2} research, and have been established as a reliable tool.
And although not directly based on the proposal by Li and associates, their questionnaire (which they claim to have been validated) and the one used in this dissertation are very similar in the type of information it collects and the methods it uses to do so.

It is important that researchers are aware of the limitations of linguistic questionnaires, but they offer a good balance between the information gathered regarding the participants' profiles, and the experimental load to which these participants are subject.
This rationale has been added to the introduction, and the discussion regarding cloze tests was moved here from the appendix as well.
\end{quoting}

\item \label{online}
Online performance:
The experiments were performed on-line.
Limitations of this procedure and alternatives that might have been adopted need discussing.
The impact that the method used may have had on the sample (any biases, lack of control over participants’ performance etc.).
As well as justifying what was done when studies are introduced, some discussion of how these decisions may affect the interpretation of the work is needed too.

\begin{quoting}
\difflink{331330a}%
Additional bibliographical references discussing the validity of online data in comparison with more traditional laboratory data were added, as well as a discussion regarding the reasons motivating the decision to use online tests in the experiments in part 1.

Based on the literature reviewed, online data is identified as possibly more variable and, therefore as having smaller statistical power.
These considerations have been mentioned in the dissertation.

However, I don't think having used different sources of data would have had a significant effect on the results, since the variance in the obtained data is comparable to that from the original study, which used more traditional controlled methods.
Still, I've mentioned that other sources of data (together with broader participant recruitment) might shed light on eg. the lack of \L{2} proficiency effects (because of the likelihood of the smaller statistical power).
\end{quoting}

\item \label{crossling}
P.61 motivates the main difference between the candidate’s work and other (inconsistent) literature.
‘Previous studies were one-directional whereas the thesis took a bi-directional approach which should allow it to provide more convincing evidence whether Japanese speakers find it easy to judge Spanish stress in different positions’.
The examiners wondered whether the candidate could have exploited this more by designing studies so that comparison across the (separate) Spanish and Japanese studies could be made.
For example, perception of Spanish stress (by Spanish and Japanese speakers) is reported in chapter 3, but the design does not allow any analyses that include language group as a factor.
This may not be possible given the design (in which case he should defend the approach taken).

\begin{quoting}
\difflink{4046656}%
This is an understandable comment on the work, and one that I share.
However, as was explained in the viva, decisions that had to be made after the study had begun resulted in conditions that would have made direct comparisons render questionable results.
Rather than this, the decision was made to base those comparisons in higher-level conclusions derived from the independent data.

A new section in the closing chapter regarding methodological limitations, and explaining these arguments, was added.
This is not presented in the text as an admission that the methodology itself is flawed, but as a statement of particulars of its implementation that are open for improvement.
A recognition that the chosen and novel bidirectional approach can be potentially rewarding, but is certainly hard to apply.
\end{quoting}

\item \label{web}
Stimuli:
The materials used and the design of the experiments are clearly described and justified in each chapter.
Audio illustrations of the stimuli presented in the tests were not available.
They would have been welcome, especially in the case of the experiment on secondary acoustic cues, since the technique adopted has limitations (pp. 160-161).

\begin{quoting}
The full set of stimuli for the second experiment has been made available online%
\footnote{%
  \url{http://pinguinorodriguez.cl/research/phd}
}%
.
Also available in that \textsc{url} is a link to the survey definitions. When the dissertation is accepted and finalised, links to the document will also likely be found there, as well as other information on the topic (anonymised data sets, the analysis scripts used, etc).
\end{quoting}

\item \label{normalisation}
The pitch track description is clear.
However, is this the best procedure (see the following description):
‘Time values were likewise normalised by setting the beginning of the utterance to zero and its end to 1, and making each syllable in the keyword occupy a third of the keyword’s duration.’
Some authors argue that non-linear time warping is necessary (see the work of Jorge Lucero, Gracco, Munhall and others).

\begin{quoting}
\difflink{99bd848}%
The method suggested by Lucero and colleagues is particularly beneficial for longer stretched of speech, and they themselves state that the assumptions of linear normalisation are sound for short stretches.
This meant that there was probably little to gain from the implementation of their significantly more complex normalisation method.
This explanation has been added as a footnote to the text.
\end{quoting}

\item \label{theory}
The four goals at the end of chapter one and those at the start of chapter 3 are very empirical.
The candidate should indicate whether they could address theoretical issues raised in the literature review (\eg to discriminate between the alternative \textsc{slm} and \textsc{pam} approaches?).
If not, state this and at some point in the thesis indicate what approach a satisfactory approach to theory would be in the light of the findings (\eg add to the short final chapter).

\begin{quoting}
Both the first and the second research questions set forth in the introduction relate to one of the key aspects raised in the literature review: whether there are grounds to say that suprasegmental and segmental categories are entirely different entities, guided by different principles and rules; or if they are fundamentally the same, regardless of whether they refer to segments or not.

The dissertation provides evidence in support of the idea that, regardless of what kind of category they are, they will behave similarly, and be affected by similar phenomena.

Results in response to the fourth question also provide evidence in support of a phonetics-based, pre-categorical model of perception, such as \textsc{nlm} or the perceptual interference account.
This point is made in the final chapter, when talking about the results for the perception of duration in particular.

Finally, the third question deals with the question of whether speakers from languages like Spanish and Japanese, which do not share a common stress class, are able to improve their ability to perceive \L{2} suprasegmental contrasts.

Although it does this synchronically, by looking at self-assessments of non-native language proficiency as a stand-in for the passage of time, evidence from the second study do suggest that even under the impoverished conditions tested in this case (with participants coming from a relatively limited range in the proficiency dimension, and using \textsc{fla} learners), there exists a significant correlation between language use in particular (more than proficiency) and performance.
This has implications for the teaching of suprasegmentals in the classroom, which are explored in the final chapter.

\difflink{10226c7}%
A more explicit connection to these issues has been added to the final chapter.
\end{quoting}

\item \label{recruitment}
`Controls, on the other hand, were recruited through internet-based participants’ social networks and local contacts in Japan.' Recruitment should be equivalent for all groups. This should be mentioned as a limitation.

\begin{quoting}
\difflink{40db285}%
This limitation has been explicitly mentioned in chapter 2, regarding the first Spanish study, and referenced again in chapter 3, regarding the first Japanese study.
In the latter case, the reference points out that the same issues applied in both cases.

In both cases, I make the argument that while certainly a limitation, it would probably not have had a great impact on the results considering the magnitude of the effects.
\end{quoting}

\item \label{controls}
Results of the four experiments reported in the thesis are accurate both in the text and in the very clear graphical presentations. Although complex statistical analyses were used, the essential facts are summarised in an accessible way for readers not familiar with statistics. That said, a more detailed discussion of the performance of the control native speakers in the perceptual experiments, together with a comparison with previous research in the section devoted to the acoustic analysis of the stimuli, would have been a welcome addition.

\begin{quoting}
\difflink{38e331b,29e9c51}
A discussion of the responses for both control groups in the first study was added to the respective chapters.
No significant effects were found for the Chilean control group, but significant effects of sentence and accent were found for the Japanese control group.
These results are described.

In addition, the possible link between the significantly higher responses for initial-accented words for the Japanese control group, and the bias towards earlier accents for the Chilean learners is briefly discussed.
\end{quoting}

\item \label{tobi}
Is \tobi\ the best approach towards annotating Japanese and Spanish. The methodological choice of a predominantly phonological annotation system (\tobi) for the representation of prominences, although explained in the text, needs some further justification.

\begin{quoting}
\difflink{8ac3a56}%
The already existing justification in the introduction was briefly expanded, and a disclaimer was added to the chapter on acoustic analysis of the stimuli, explaining why \tobi\ was used,
Part of this explanation is based on the fact that, despite its use, no claims are made regarding the phonological status of the tones described.

\Tobi\ is not a phonological annotation framework, as much as a system for prosodic annotation, and there is a precedent for its use in the description of intonation at a level that is above that of the phonology, as ``an equivalent to the \textsc{ipa} for segments''.
Incidentally, this is the stated goal for the system, as put forth in its seminal paper.

Still, the comment is a welcome one, and any future papers that are written based on the results of this dissertation will most certainly include more directly phonetic-based analysis, such as the Smoothing Spline \textsc{anova} discussed in the viva.
\end{quoting}

\item \label{momo}
Significant for all conditions save /momo/. Does the candidate have any explanation why this was not significant?

\begin{quoting}
\difflink{cb50d2d}%
What was not significant in this case was the correct identification rate for both terms.
The reason why this was significant in the other cases was because of a bias towards initial-accented words in the initial- versus final-accented words contrast, and the great difficulty participants encountered when identifying unaccented words in a final-accented versus unaccented contrast.

In the case of /momo/, the contrast is one between initial-accented and unaccented, which are diametrically opposed in terms of the behaviour of their \fzero\ contours.
It is likely that this greater distance has made this contrast more auditorily salient than the other two, making correct responses for both terms similar and therefore not significantly different.
This explanation has been made more explicit in the dissertation.

Also added in that explanation is the idea that, if this is the case, then it could and should be tested by looking at auditory discrimination (see \cref{discrimination}).
\end{quoting}

\item \label{tandem}
Morphing software was used instead of systematically varying cues on a synthesizer. The morphing software did not allow intensity to be varied (p.160). More importantly, the categorisation results show that morphing at one end of continua was more successful than at the other. It is possible that this could affect judgements by speakers of different languages. This needs considering and discussing.

\begin{quoting}
Because of the way the stimuli were constructed, it was expected that stimuli at different ends of the continua would elicit different performances. The reason for this is that for each continuum, duration was fixed at one of the ends, meaning that stimuli at that end would have their cues more in accordance than those at the other end, in which the gap between what was suggested by the duration and what was suggested (primarily) by \fzero\ was greater.

The fact that this effect is so much greater in Spanish than in Japanese supports the idea that this is an effect of differences in duration, and not necessarily evidence of an artifact of the manipulation of the signals.
\end{quoting}

\item \label{supervision}
P.167 Participants were not supervised. This might lead to poor data (see \cref{recruitment}).

\begin{quoting}
\difflink{91d62ec}%
Participants in the experiments of part 2, which is being reported in this chapter, were supervised, unlike those of the experiments in part 1. The alluded sentence makes reference to the participants from part 1, and a clarifying sentence has been added to stress this difference.
\end{quoting}

\item \label{boundary}
The candidate used \textsc{pse} (the 50\% point) as a measure of the category boundary.
However, the fact that one end of the continuum did not seem to have clearcut categorization (see \cref{tandem}) means that 50\% points could not be estimated for some participants.
This is potentially problematic in general (not being able to estimate statistics for everyone) and here in particular because it could differentially affect results between native versus non-native languages.
This at least needs to be mentioned as a limitation.
A different percentage value could be used and results recalculated.
For instance, Hirsh (1959) used 75\% correct as is commonly done in psychoacoustics.

\begin{quoting}
Results for the second Japanese study proved that the proposed methodology was insufficient.
But this was the case not so much because of errors in the estimates of the threshold for the categorical boundaries, but because of the responses for the non-native participants tending towards a flat line at chance level.
Under those conditions, the main conclusion does not seem to be that the methodology needs to be adjusted, as that it needs to be completely re-worked.

Furthermore, because of the stated design of the dissertation (which tried to keep the tests in both languages mirroring each other as closely as possible), changing the parameters for one of the tests was considered to be outside the scope for the presently proposed approach.
And the fact that the test showed good results for the \textsc{js} participants in chapter 6, made it hard to justify retroactively modifying the parameters of both tests.

In my opinion, the very fact that the methodology collapsed in the Japanese test (but significantly not in the Spanish test) is a worthy result.; This because it highlights the extent of the difference between populations, and the need for follow up tests to explore the issue with an altogether different approach.
Possible future approaches should take into account, among other things, the compression in the response space of non-native participants, which was already apparent in the results reported in chapter 4, and explore why the identification of unaccented words is so low.
As stated in \cref{momo} and in \cref{discrimination}, one such possibility is to run a test focusing on the ability of native Spanish speakers to auditorily identify unaccented words as distinct from final-accented ones.
\difflink{cbd8f34}%
This has been added to the discussion of chapter 6.
\end{quoting}

\item \label{slope}
P.172 reports significant differences on categorization boundaries and slopes.
The native speakers probably identify the ‘poor’ endpoint (see \cref{momo}) as a clearer instance of its intended category than did the non-native speakers.
This should be checked.
If so, the slope estimate would appear to be an appropriate measure for the native speakers.
However, this may not be the case for the non-native speakers because fewer crossed the 50\% point and their data were excluded.
Use of other percentages may prevent exclusion and allow more robust analyses (\cref{boundary}).
In addition, consider whether slope is the operative variable here.
Perhaps non-native speakers need more extensive continua (hypo stimuli).
Extent of continuum range that results in given levels of categorization performance might be a better statistic for comparing speaker groups than slope of the categorization function.

\begin{quoting}
Data was excluded for some non-native participants, but only for the calculation of category boundaries, and this in itself can be considered to be a meaningful result (in that it shows that the native and non-native groups behave significantly differently).
The calculation of slopes was not affected by this, and the only result that the low performance of non-native participants had was that the values for those participants approached zero.

It is true that slope might not be the operative variable in this case, and the use of continuum range as a measure remains a valid possibility for further exploring the data collected from the second Japanese study.
Future reports on this data will likely benefit from the addition of those additional measures.
\end{quoting}

\item \label{discrimination}
Slope was used as this is a feature of categorical perception.
Discrimination also has distinctive properties for categorically-perceived continua.
Why wasn’t discrimination performance tested?

\begin{quoting}
The main reason is that it was assumed that difficulties in perception were the result of linguistic phenomena, and not an effect of problems in auditory discrimination.
That is why the first task looked at effects of sentence structure and prominence position within the word.
This was based on the performance of the Japanese participants in the Kimura study, and also on the relatively high performance that has been attested for Spanish speakers regarding perception of prosodic contrasts.

Besides, There is evidence suggesting that discrimination tasks inflate the performance of non-native listeners because they allow them access to additional non-linguistic information (Dupoux et al. 2008).

This explains why identification tasks were chosen initially.
However, some of the results in the dissertation do suggest an effect of auditory discrimination issues (see \cref{momo}).
Further exploring these matters with discrimination tasks is one of the immediate extensions that I would like to pursue.
\end{quoting}

\item \label{ceiling}
The Kimura data (p.60) and the candidate’s own data (\eg around pages 106 and 128) show ceiling effects in some conditions.
This can lead to spurious interactions as there is no margin for increase when there is a ceiling effect whereas there is when performance is lower.
Although the candidate did not do analyses with language as a grouping factor (arguably he should, see \cref{crossling}) some discussion is nevertheless warranted.

\begin{quoting}
\difflink{29e9c51}%
As part of the description of the results from the control groups (see \cref{controls}), the presence of ceiling effects in the responses from the Japanese control group has also been added.

This is, however, only a brief mention, since the worry regarding ceiling effects is that they will hide main effects that would otherwise be apparent (and therefore have the chance of creating interactions that should be there).
Although the point has been made that cross-linguistic analyses would have been welcome (a point with which I agree), the changes made to address the comments in \cref{crossling} explained why they were not possible with the design as was implemented for this dissertation.
Since no cross-language interactions were examined, this problematic aspect of the ceiling effects is not a problem.

\marginpar{7a1acf7}%
Interactions between the control and non-native groups within a language would have been possible, but were not part of the original design.
If they had been part of the design, then measures would have been taken to ensure that ceiling effects were avoided.
However, with the existing design, based on the one in Kimura et al. (2012), this was not the main focus.
This clarification was also added to the introduction.

Needless to say, this is an exciting avenue for further research, particularly in light of the main effects found for the Japanese control group.
\end{quoting}

\item \label{limitations}
The conclusions provide a good overview of the thesis and coherently summarise the answers obtained for each of the research questions and they acknowledge some of the limitations of the studies.
There are interesting remarks on the teaching of suprasegmentals in the final pages (pp. 203-204).
This topic which might be addressed in more detail.
For instance, the discussion should include a section on this topic (from the perspective of both languages).

\begin{quoting}
\difflink{5b745db}%
Added considerably more references and expanded the discussion of the teaching of suprasegmentals as a second language.
No extra section was added to the final chapter since the section where teacher attitudes were discussed originally already seemed like the natural place to have this, but this section is greatly expanded.
\end{quoting}

\end{enumerate}

\end{document}
