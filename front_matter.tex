\maketitle

\makedeclaration

% Collophon
\thispagestyle{empty}
\vspace*{\fill}
\begin{center}
  \begin{minipage}{0.7\linewidth}
    \scriptsize
    \centering
    This dissertation was typeset using \XeLaTeX.\\
    The main font in this document is Linux Libertine. Margin notes are set in Linux Biolinum, and monospaced text is set in Inconsolata.\\
    Japanese text is set in Takao Mincho.\\
    PGFPlots and Inkscape were used to generate plots and figures.\\
    The fleuron ornament was provided by \href{http://www.vectorian.net}{Vectorian}.\\
    The digital version of this document contains no raster graphics.\\
    This dissertation was made entirely with Free Software.\\
  \end{minipage}
\end{center}

\clearpage

\begin{abstract}

Perception of novel phonetic contrasts in a second language has been studied extensively, but suprasegmentals have seen relatively little attention even though difficulties at this level can strongly impact comprehension.
Available studies suggest that the perception of segmental and suprasegmental categories are subject to similar factors, but evidence is not entirely conclusive.
Additionally, studies focusing on the perception of lexical prominence have suggested that perception across languages with different accent types might be particularly problematic, meaning that these effects in particular would be independent of the direction of language learning.

This dissertation explores both of these questions by studying the perception of lexical prominence by second-language learners in Spanish (a stress-accent language) and Japanese (a pitch-accent language).
Following a bidirectional approach, it examines whether the perception of phonologically different types of lexical prominence is subject to similar effects as those traditionally identified for cross-linguistic segmental perception, and how these relate to the direction of learning.

A first set of studies provides a comparative acoustic description of prominence in both languages, and presents the results of an identification task with natural words in different positions within a sentence.
Using multiple speakers, these tests showed that the difficulties seen by both groups are different and related to features in their \gls*{L1}, and that despite phonological differences, contexts existed in which high performance was possible.
A second set of studies explored the sensitivity of non-native listeners to secondary acoustic cues and the development of new accentual categories, and showed effects of learning for both groups and a strong sensitivity to duration for learners of Spanish.
Learners of Japanese showed extremely poor category development for unaccented words in particular.

Overall results show that existing research on \gls*{sla} is applicable to suprasegmental perception, and that the transfer effects affecting both groups have different domains and scope.
The implications for language teaching and theories of \gls*{L2} perception are discussed.

\end{abstract}

\begin{acknowledgements}

Nothing could have prepared me for what doing a PhD would actually entail.
The time spent, the uncertainty, the insecurities, the failures and dead ends\ldots\ and at the end, the elation and relief of a work well done.
If I had a data point for each time I stopped and thought that the whole endeavour was hopeless; for each time I changed my mind and thought that it was not only worthwhile but extremely exciting; for each time I looked back and thought that something should have been done differently, my results would have much more statistical power.

Definitely the most humbling part of the entire experience was to realise just how many people are needed to finish a PhD, how many lives are touched in the process, and how thankful you feel for all the ways in which the people around you helped you get to the end.

This is what makes an acknowledgement section so necessary, and why writing one seems so futile: because for every person that is mentioned by name, there are 10 more that are being left behind.
Knowing this, this is my attempt to thank all those without whom this would have been a failure.
If you think you deserve a place here and cannot find your name, I hope you will forgive my oversight.
Next time we meet, the beer is on me.

% UCL
First I need to thank
  Valerie Hazan, for her undying support and attention, and her most valuable advice throughout.
In the four years (and a bit) that I had the privilege to work under her supervision, she always had her door open, and was always willing to solve my problems and read over my drafts.
The comments I received in return were as harsh as they needed to be, never more, and always helpful.

I would also like to thank
  Paul Iverson, for his expertise and his \emph{take-no-prisoners} approach to giving advice, and to
  Mark Huckvale, for his advice on intensity normalisation strategies.

Paul is also largely responsible for my spending a year at UMass Amherst, where professors
  John Kingston and
  Kristine Yu took me in and offered a completely different perspective to the things I had been doing, and had yet to do.
I have particularly fond memories of the last conversation I had with John before heading back, and I only wish I can express how lucky I feel for having had the opportunity to be in his lab.
The students in the Linguistics department also made me feel welcome and challenged, and I cherish the friendships I made while with them.
We should go climbing again.

In Sophia University I also received the help of professors
  Arai Takayuki and
  Kimiyo Nishimura, who not only provided me with a place to work, but assisted in the data collection in ways I did not think would be possible.

% UC
Thank you also to the people at Universidad Católica, and in particular to
  Domingo Román for his constant encouragement, his friendship, and his unwavering faith in me.

\textsc{Ucl} and \textsc{sh}a\textsc{ps} (with comma or without) became for me so much more than a place to work, and having to move on is the one part of completing my PhD that makes me sad.
% Hazan Lab
Thank you in particular to the members of the lab:
  Sonia Granlund,
  Michèle Pettinato,
  Outi Tuomainen,
  Wafa Alshangiti, and
  Lou Stringer; and
  Yasuaki Shinohara, who only realised he could call himself a member as he was leaving \textsc{ucl} to bigger and better things.
% PhD
The rest of the graduate students are the main reason my stay in \textsc{ucl} has been as memorable as it has.
A special thank you to
  Melanie Pinet,
  Georgina Roth,
  Albert Lee,
  Lucy Carroll,
  Tim Schoof,
  Cristiane Hsu,
  Mauricio Figueroa Candia,
  Kurt Steinmetzger,
  Emma Brint,
  Yue Zhang,
  Jieun Song,
  Petra Hödl;
my housemates
  Faith Chiu and
  Gisela Tomé Lourido; and
  Mark Wibrow, without whom this dissertation would have been significantly uglier (and less fun to write).

My gratitude goes also to my thesis examiners, professors
  Peter Howell and
  Joaquim Llisterri, who helped make the last part of the PhD the most exciting and rewarding of all.
They offered me their sincere advice and made significant contributions to the final stages of this little monster.

% UdeC
A special mention is also due to those who participated in my admittedly tedious tests, without whom no science would have been possible; and to those who so efficiently helped me in collecting data for the latter stages in my research, when data collection is the last thing one wants to do.
Thank you to
  Karina Cerda,
  Rosa Catalán and
  Imran Hossain in Santiago;
  Hernán Acuña in Concepción; and
  Misae Kunitake and
  Kazuaki Yasuki in \Tokyo.
Thank you also to the people at \textsc{ceija} and the Instituto Japonés de Cultura in Chile, and to professor Edinson Muñoz at Universided de Santiago, who graciously allowed me access to their students.
I am in your debt.

% TUFS+
I am also grateful to the professors who I met while at \textsc{tufs}, and in particular to
  Shigenobu Kawakami, who hosted me while there, and
  Takuya Kimura, with whom a productive and rewarding academic collaboration lead eventually into this dissertation.
That work would not have been the same without the rest of the members of that team: professors
  Hirotaka Sensui,
  Atsuko Toyomaru and
  Miyuki Takasawa.
Professor Noriko Nakamura also offered her helpful advice on the teaching of intonation in Japanese and the search for appropriate Japanese words to use.
She is also responsible for much of my initial knowledge of the Japanese accent.
Many thanks to you all.

Thank you also to my family, and friends, who helped me in all the other ways in which company and support can be helpful.
Even in the distance, I never stopped feeling your love.

Finally, thank you to
  Koto Sadamura, my partner in crime and love, for the support, the motivation, and the visions of a brighter future.
Not only did you make this possible, but you also made it so much more enjoyable.
You are the reason all of this made sense.

\end{acknowledgements}

\tableofcontents
\listoffigures
\listoftables
\printglossaries

\cleardoublepage
\thispagestyle{empty}
\renewcommand{\epigraphsize}{\large}
\setlength{\epigraphwidth}{0.6\linewidth}
\vspace*{8\baselineskip}
\epigraph{%
  You can't always get what you want\\
  But if you try sometimes,\\
  You just might find\\
  You get what you need%
}{%
  \textit{You Can't Always Get What You Want}\\
  \textsc{The Rolling Stones}%
}
