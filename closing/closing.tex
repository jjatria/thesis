\part{Closing}

\chapter{General discussion}
\label{chp:discussion}

The present dissertation set out to investigate four main research questions.
\begin{enumerate}
  \item \label{q:segmental} Do the effects of perceptual interference in \gls*{L2} learning shown at the segmental level occur as well at the suprasegmental level in the acquisition of \gls*{L2} lexical stress?
  \item \label{q:cue_weighting} Is the weighting of \gls*{L2} acoustic cues of suprasegmental categories affected by that of \gls*{L1} cues, as it happens for segmental categories?
  \item \label{q:proficiency} Does \gls*{L2} language proficiency have an effect on the perception of \gls*{L2} lexical prominence in \gls*{fla} learners?
  \item \label{q:bidirectional} Are the transfer effects affecting cross-linguistic perception of lexical prominence independent of the direction of learning?
\end{enumerate}
Considering the strong effect that prosody has on comprehensibility and foreign accentedness, it is surprising that some of these questions have been as understudied as they are.
And although there is evidence that training, age and transfer effects exist for the perception of \gls*{L2} prosody (see \cref{sec:segmentals_to_suprasegmentals,sec:proficiency}), the relatively small body of work, and the limited scope of its focus, has meant that it is still not clear whether the existing models of speech perception are entirely applicable to the perception of a considerable portion of what constitutes speech.
Recent efforts on this line do exist (\eg~\cite{so+2008, so+2011}), but more research is necessary.
This dissertation aims to contribute to this conversation.

\section{A bidirectional approach}
\label{sec:closing_bidirectional approach}

One of the defining characteristics in this dissertation was the explicit effort of examining simultaneously the perception of suprasegmental traits from the perspective of \glsdesc*{sj} and \glsdesc*{js}.
The main reason to pursue this approach was to examine the specific perceptual difficulties affecting each population with a shared methodology, in a way that made it possible to identify what they had in common.
The proposed methodology was a result of trying to answer specifically research question \ref{q:bidirectional}, and to explore what this might tell us about language perception in general, and about the perception of suprasegmental categories in particular.
The findings relating this specific issue are presented below, in \cref{sec:closing_phonetic_similarities}.

But before moving onto that, and since this bidirectional approach is novel, some words must be said about its costs and benefits.

As the studies in this dissertation progressed, it became increasingly clear that the added complexity in the experimental design would be one of the most serious drawbacks of the approach.
A shared methodology, particularly with aspects of two languages that are so fundamentally different at a phonological level, means that the aspects that will be interesting to examine in one case might not be the most appropriate in the other.
The methodology that was chosen for the second experiments, reported in \cref{prt:continuum} was a way to address this problem, by choosing an approach that would support an analysis at multiple levels and make it possible to focus on the results that were of main interest in each case: the effects of duration as a secondary cue for \gls*{js} learners, and the lack of appropriate non-native accentual categories for \gls*{sj}.
But even then there was a cost to pay with regards to the loss of specificity in the design.

However, this common methodology also made it easier to realise that the differences in the perceptual problems affecting both language groups could be explained as fundamentally the manifestation of the same underlying principle: that the perception of suprasegmentals, just as that of segments, is governed primarily by phonetic similarities.
This is indeed the notion that is at the core of the \gls*{nlm} model, but even the so-called ``categorical'' models (\gls*{slm} and \gls*{paml2}) emphasise the relevance of phonetic similarities in the perception of segments (see \cref{sec:transfer_effects}).

\subsection{Methodological limitations}
\label{sec:closing_limitations}

It is perhaps surprising that a dissertation that places such a high emphasis on the contrast between two different populations of language learners, spends so little time making direct comparisons between the two.
This is a direct results of the problems described above regarding the difficulty in the design of equivalent experiments.

In particular, the initial choice of minimal trios to study Spanish stress placement --- a decision that was based on the experiences of a long tradition of similar studies in Spanish~(\cite[\eg][]{enriquez+1989, llisterri+2003, llisterri+2005, alfano+2008, schwab+2011a, schwab+2011b}; \cite[and specially][]{kimura+2012}) --- was specially restrictive in Japanese because of the small number of minimal sets that fit those requirements (see \cref{sec:1_japanese_speech_materials}).
This resulted in the forced choice of different sets of materials for both languages, and severely limited the possibility of making direct comparisons between both language groups.
Particularly since a large part of the results for Japanese reported in \cref{chp:1_japanese} resulted from minimal pairs.

In order to address this limitation, comparisons between language groups were done at a higher level, one step removed from the data itself, and based on the conclusions that could be derived from the results of the different studies.
\marginpar{A note for future studies}%
Further studies would be welcome that expand and improve upon this methodology, addressing this issue in a way that allows for the direct comparison between groups.
This is a task that, while difficult, is likely to yield interesting and novel results.

\subsection{Phonetic similarities are direction-independent}
\label{sec:closing_phonetic_similarities}

The studies in \cref{prt:identification} provided an overview of the perceptual difficulties affecting the perception of lexical prominence for both language groups, gathering information that is completely novel up to this point.
The results from those studies showed that while \gls*{js} participants were affected primarily by the effects of sentence intonation and not so much by prominence position; the effects shown by \gls*{sj} participants were the exact opposite, with prominence position (and type) being responsible for the largest part of the difference, and the effects of sentence intonation being much smaller.

While the explanation for these results can be made based on what the source and target languages are, they can also be explained by focusing on the aspects that these contexts have in common.

In both languages, the contexts with the highest performances describe the conditions under which prominence is realised in the most similar way: focusing on the action of \fzero, the only common cue, prominence is produced with a peak-like \fzero\ contour that is aligned either at the end of the prominent syllable or during the first half of the one following (see \cref{sec:spanish_cues} and \cref{sec:japanese_accent_types}, and \cref{sec:1_acoustics_pitch} for data for stimuli in this dissertation).
In these cases, prominence is always realised with an \HstarL\ tone.

The problematic contexts, on the other hand, can be predicted by identifying the circumstances under which each language allows for variation in the realisation of prominence.
In the case of Spanish, which has a multiplicity of cues, prominence can be realised in a number of different ways (see \cite{fernandez+2003, estebasvilaplana+2010, ortiz+2010}, and the inventory in \cref{fig:spanish_pitch_accent_comparison}).
But this variation does not depend on the position of the prominence within the word: it is almost entirely governed by the effects of sentence intonation, which is allowed to exert an influence on the intonation of the word because intonation is not needed to mark the position of the Spanish stress \parencite[][among others]{llisterri+2005, ortegallebaria+2007}.
In the case of Japanese, on the other hand, the reliance on \fzero\ alone as the acoustic correlate of prominence has made it robust against changes in sentence intonation, but allows variation depending on the type and position of the prominence.

As detailed in \cref{sec:transfer_effects}, both \gls*{slm} and \gls*{paml2} can be said to be ``multi-levelled'', with speech perception being the result of an interaction between phonetic and phonological similarities.
But results from \cref{prt:identification} seem to align better with the precepts of \gls*{nlm}, for which perception is pre-categorical, and therefore much lower-level and centred on the acoustics.
The perceptual interference account proposed by \textcite{iverson+2003, iverson+2008}, which shares a close similarity with \gls*{nlm}, might provide a useful interpretation of the results, in particular those that relate to the sensitivity to duration, as will be discussed below.

\subsubsection{Non-native sensitivity to secondary acoustic cues}
\label{sec:closing_duration_sensitivity}

The studies reported in \cref{prt:continuum} were designed to simultaneously test the sensitivity to duration as an acoustic cue (based on the results presented in \cref{chp:1_spanish,chp:1_japanese}), the effects of the participants' \gls*{L1} on the weighting of those cues, and the degree of development of accentual categories for both populations (as measured by the steepness of their response curves).

Results from \cref{chp:2_spanish} found a very clear effect of duration for \gls*{js}, who not only were able to attend to changes in duration but also responded to them in ways that were extremely similar to those of native speakers.
Boundary positions were very similar for both groups, and exactly the same for most conditions.
But perhaps the strongest evidence of the degree of similarity between native and non-native responses was the effect caused by the conflicting cues in the synthetic stimuli, which was identical.
And although no significant effects of duration were found in \cref{chp:2_japanese}, a trend shown by non-native learners of Japanese showed that maybe in their case there is also a sensitivity to duration, even though duration is not useful in Japanese (nor particularly significant in their stimuli).

Although similar cases of non-native learners relying on duration in particular for discrimination, even when it was not a native cue, have been previously reported in the literature \parencite{bohn1995}, these results could be explained by once again relying on the perceptual interference account.

\marginpar{Perceptual interference}%
The core precept behind the perceptual interference account is that perception is primarily phonetic in nature, and coloured by differences in the scaling of the perceptual space.
It shares with \gls*{nlm} the idea that difficulties in the perception by non-native learners is caused by the perceptual space being literally warped, making certain sounds appear to be more or less distant from each other.
However, unlike \gls*{nlm}, it makes fewer assumptions regarding what is the cause of this warping.
While \gls*{nlm} attributes the warping to the ``language magnet'' effect of linguistic prototypes, around which phonological categories would develop, and which are the result of the statistical distribution of sounds in the input primarily during childhood, perceptual interference does not make direct claims as to what explains the distortion: it simply attempts to explain the way in which the distortion affects perception.

When examining the perceptual difficulties faced by Japanese learners of English in the acquisition of the /r/-/l/ contrast~\parencite{iverson+2003}, the authors found that while the non-native learners showed a relatively high sensitivity to \ftwo, this was a sensitivity that was useless for the discrimination between /r/ and /l/, which are primarily distinguished by native speakers by means of differences in \fthree.
They posited that their difficulty in identification stemmed from the fact that the distortions they had on their perceptual space were not aligned with those that were useful for the relevant contrast, which were evident in both the native speakers and an additional group of German learners of English, who also performed highly.

This provides a good explanation of the results obtained from \cref{chp:2_spanish,chp:2_japanese}.
If the trend shown by \gls*{sj} had turned out to be significant, it would have been the result of a high sensitivity of the Spanish speakers to duration changes for the perception of accentual contrasts, a sensitivity that, like that of the Japanese learners of English, made them attend to what in this case were the minute differences in the duration (or any other feature) of the stimuli.
But perceptual interference can also explain the results for the \gls*{js}, since it has already been shown that sensitivity to duration is easily acquired, but also because Japanese \emph{does} have a sensitivity to duration, although it's exclusively used for the discrimination between short and long vowels.

On the other hand, this does not directly account for the low performance shown by \gls*{js} with unaccented Japanese words in non-final contexts, which, as was discussed in \cref{sec:1_japanese_pitch}, behave like accented words in those contexts.
When this happens, they are realised like a word accented in the third syllable, which makes it possible that the low performance shown by the participants is related to the bias that was shown by non-native learners towards the perception of prominence earlier in the word.
It is not entirely clear what mechanism is responsible for this, and indeed there is no guarantee that a single process can be held accountable for both of these effects.

This poses a number of interesting extensions to these studies, exploiting the fact that both populations have a perceptual sensitivity to duration, and that both make use of duration for contrasts in production (although in both cases with different ends).
The interaction between the segmental and suprasegmental levels in this regard is also intriguing, since \gls*{sj} learners often report having particular trouble perceiving the Japanese vowel length contrast even though they are clearly sensitive to duration.
This might be the result of the segmental duration cue being interpreted as a secondary suprasegmental cue for prominence, where it would be masked by differences in pitch.

\subsubsection{Segmental and suprasegmental categories}
\label{sec:closing_segmental_suprasegmental_categories}

Although research on the perception of non-native contrasts has been extensive, it has been well established in this dissertation and elsewhere that the field of suprasegmental perception has been left wanting (see \cref{sec:segmentals_to_suprasegmentals}).
In particular, this has implications regarding the applicability of the perception models discussed in \cref{sec:introduction_model_comparison}, which up to this point has been a contested topic.

\Textcite{so+2008, so+2011} are among the few to have made a direct attempt at answering the question of whether the knowledge acquired from these models applies to the perception of suprasegmental categories, and if so how well.
But even though their results are promising, by calling their categories \emph{iCategories} they use a terminology that unfortunately implies the very thing against which their results seem to lead.

The studies presented in this dissertation did not set out to directly test the predictions made by the leading perception models discussed above.
But they do present evidence in support of the idea that segmental and suprasegmental categories, insofar as they are linguistic categories, seem to behave similarly and be affected by similar phenomena in similar ways.

Further studies in these areas would be welcome.

\section{Effects of language proficiency}
\label{sec:closing_language_proficiency}

Studies on both \cref{prt:identification,prt:continuum} examined the effects of language proficiency in the perception of lexical prominence, and while the first set of studies was not able to find any correlations, studies in \cref{prt:continuum} seemed to have been much more sensitive, and found some significant correlations.
Results on \cref{chp:2_spanish,chp:2_japanese} reported significant effects of the proficiency measures, and in particular of language use, such that participants who reported using their \gls*{L2} more often behaved more like native speakers, having earlier category boundaries (\gls*{js} learners) and steeper category slopes (both groups).

These results are in line with other reports of improvement in the ability of \gls*{L2} learners to perceive suprasegmental contrasts after training~(\cite[\eg][for French learners of Spanish]{schwab+2011a, schwab+2011b, schwab+2012, schwab+2014}; \cite[and][for Italian learners of Spanish]{alfano+2007, alfano+2008, alfano+2010}; as well as additional coverage in \hyperref[sec:second_language_acquisition]{the introduction}).

The results reported in this dissertation are particularly encouraging, because unlike most previous studies, participants considered here were \gls*{fla} students who had never stayed for a considerable amount of time in an environment in which the \gls*{L2} was dominant.

But one should be cautious, since it is still possible that the correlation that existed between \gls*{L2} use and performance, while significant, might be due to other factors.
This seems to have been the case in \textcite{saalfeld2012}, which reported the results of a training experiment in which English speakers were trained in the perception of Spanish lexical contrasts and tested in an indirect word recognition task, in which a target word could be interpreted in one of two different ways depending on the position of the prominence changing the meaning of the framing sentence, and participants were asked to identify the meaning of the sentence.
In that study, although participants from the experimental group showed an improvement, an even larger improvement was shown by the control group, who received no special prosodic training whatsoever.
As possible causes, they mention differences in participant motivation (the control group claimed to be more highly motivated) and the lack of distractors in the test (which meant that the test itself might have served as training), highlighting the need for further detailed studies controlling as much as possible the large number of confounding variables involved in these tasks.

\marginpar{Teacher's attitudes towards prosodic training}%
Just like the participants in \textcite{saalfeld2012} might have been influenced by attitudinal differences in their approach to the learning process, language teachers are also affected by their own attitudes towards the teaching of pronunciation.
Indeed, part of the motivation for examining the effects of language training was the realisation, through personal communication with language teachers active in Chile, Japan and the United States, that a large majority of them did not, in fact, spend any considerable amount of time specifically teaching students how to provide prominence to a given syllable, or how to best perceive where prominence had been assigned by other speakers.
Similarly, conversations in particular with learners of Japanese showed that a large number of them were never made aware that the language had an accentual system, much less how to correctly interpret the acoustic cues that encoded it.

The reasons provided by language instructors to prefer not to teach intonation could be broadly grouped into two categories:
\begin{enumerate}
  \item That they preferred to employ their limited resources in the training of other aspects of the language which were more important to ensure the students' ability to express themselves and be understood in their \gls*{L2}.
  \item That there was a general lack of training methods and tools to effectively improve the perception of these prosodic features among their students.
\end{enumerate}
And these same attitudes regarding the difficulty and questionable relevance of prosodic training are widespread in the teaching community, at the very least among teachers of both English~\parencite[\eg][among others]{burgess2000, macdonald2002, breitkreutz+2009, derwing+2005} and Spanish~\parencite[\eg][among others]{uso2009, orta2009, orta2010} as \gls*{L2}.

There are a number of reasons for this to be the case, including the reticence shown by teachers to change their established practices~\parencite{orta2010}, and the lack of knowledge and confidence in relevant teaching methods~\parencite{macdonald2002}.
In particular the latter of these is strongly connected to the lack of impact of pronunciation research mentioned before, and to the gap that it generates between the knowledge reported in the literature and its application in the real world.
Specifically, the first issue has been dealt with in detail in \cref{chp:introduction}, which presents some of the extensive evidence in support of the effect that that pronunciation --- and in particular suprasegmental pronunciation --- has on foreign accentedness, intelligibility, and comprehensibility.

As for the second issue, part of it stems from the very same gap that was mentioned above between the research and the practice of \gls*{L2} instruction.
This is particularly the case with the survival of outdated teaching methods of limited effectiveness and the lack of knowledge or instruction on more research-driven approaches \parencite[see][for a review]{orta2010, gilbert2014}, and with the unsuitability of a large proportion of current teacher training programs, which do not properly prepare them to make use of existing methods and tools \parencite[see][for a review]{murphy2014}.
\marginpar{Development of teaching tools}%
And although teachers complain about the lack of tools, recently there has been considerable interest in the development of new technologies that can be used for language teaching in general \parencite[see][for a review]{eskenazi2009}.
This is particularly the case with Japanese, with the recent development of tools aimed specifically at the improvement of intonation providing immediate feedback on its production, like the \glsdesc*{ojad} (\gls*{ojad}, \cite{minematsu+2012, nakamura+2013, hirano+2013}; and \cite[see also][]{hirose+2003}).

And although the efforts are considerably less well-concerted, this is true for Spanish as well, with several researchers reporting on the use of speech manipulation strategies to train students in the production of specific language features.
This is the case with \citeauthor{lahoz2012}, who used a \citetalias{praat}-based tool to automatically convert students' productions to so-called \emph{sasasa} speech in order to train the perception of rhythm and stress placement~\parencite{lahoz2011, lahoz2012}.

A different kind of proposal is that made by \textcite{corominas2014}, who also focuses on the use of \citetalias{praat} in the classroom, but instead of automatic manipulation tools describes strategies with which the program can be used to illustrate specific mispronunciations or deviations in the students' production in a much more hands-on (and therefore resource-intensive).
Although they offer guidelines and some instruction on the use of the program, the lack of ready-made tools will limit the applicability of these methods.
But despite these limitations, there are numerous reports of language instructors using \citetalias{praat} for similar approaches~(\eg~\cite[][for Iranian students of English]{gorjian+2013}; \cite[][for Japanese students of English]{wilson2008}).

Results of this dissertation contribute to the discussion by providing clear evidence that, at least for isolated words, non-native learners studying under conditions that are far less than ideal --- without any targeted training or an \gls*{L2}-predominant environment that can increase their exposure --- show a link between their proficiency and their ability to perceive non-native suprasegmental contrasts.
What remains is to examine in more detail the magnitude of that benefit, and the ways in which training can be improved so that its effects are longer lasting and have the broadest scope possible.

\fleuron
