%% Thesis XeLaTeX style 
%%
%% This style file is free software: you can redistribute it
%% and/or modify it under the terms of the GNU General Public
%% License as published by the Free Software Foundation, either
%% version 3 of the License, or (at your option) any later version.
%%
%% The testsimple plugin is distributed in the hope that it will be
%% useful, but WITHOUT ANY WARRANTY; without even the implied warranty
%% of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with selection. If not, see <http://www.gnu.org/licenses/>.
%%
%% Copyright 2012-2016 Jose Joaquin Atria

\usepackage{xparse}
\usepackage{xspace}
\usepackage{xstring}
\usepackage{ifthen}

%% Declare style options
%%
%% bw        Typeset thesis in black and white mode
%% color     Typeset thesis in colour (set by default)
%% printer   Minimise color in thesis, using colored figures only
%% external  Externalise figures (with PGFPlots, set by default)
%%
%% If external is set, figures will be externalised, and pregenerated
%% figure files will be used. Otherwise, the figures will be processed
%% normally, with no externalisation.
%%
\newboolean{external}
\DeclareOption{external}{
  \setboolean{external}{true}
}

%% When in black and white mode, an alternative colorspace is used for
%% figures, and reference and other typesetting colours are disabled. Using
%% this mode (instead of printing the colour version in grayscale) gives
%% more control over what colours to use when, and generally better
%% results.
%%
\newboolean{bw}
\setboolean{bw}{false}
\newboolean{printer}
\setboolean{printer}{false}
\DeclareOption{bw}{
  \setboolean{bw}{true}
  \setboolean{printer}{true}
}
\DeclareOption{color}{
  \setboolean{bw}{false}
}
\DeclareOption{printer}{
  \setboolean{printer}{true}
}

% Set defaults
\ExecuteOptions{color}
\setboolean{external}{false}

\ProcessOptions\relax

%%
%% Page layout
%%

%% Margins
%%
%% The UCL template specifies A4-sized paper, but is less specific about
%% margins. Page layout is taken from Bringhurst's The elements of 
%% typographic style (p.175, based on the golden section).
%%
% Override page offset (set in ucl_thesis)
\setlength \hoffset{0pt}
\setlength \voffset{0pt}
% Use geometry for page layout definition
\usepackage{geometry}
\geometry{
  a4paper,                           % 210 x 297 mm
  twoside,
  inner          = 0.111\paperwidth, % 23.333mm + bindingoffset?
  top            = 0.111\paperwidth, % 23.333mm
  bottom         = 0.222\paperwidth, % 46.666mm
  outer          = 0.222\paperwidth, % 46.666mm
  marginparsep   = 5mm,
  marginparwidth = 7em,
  footnotesep    = 5mm,
  headheight     = 14pt,
  % showframe
}
% Is this necessary?
\geometry{
  bindingoffset  = 0.111\paperwidth, % 23.333mm
}

%%
%% Floats
%%

% Allow larger floats in text pages
\renewcommand{\topfraction}{0.9}  % max fraction of floats at top
\renewcommand{\bottomfraction}{0.9} % max fraction of floats at bottom

% Go for broke processing multiple floats
\extrafloats{100}

% Don't use wider spaces before starting a new sentence
\frenchspacing

% Don't force page bottoms to be flush
\raggedbottom

%%
%% Folios
%%
%% Bringhurst's page layout has two possible positions for folios: at the outer
%% bottom corner, flush with the outer margin and below the text block, or in
%% the outer margin. Since the margins are used for margin notes, the bottom
%% option is used.
%%
\renewcommand{\footnoterule}{\noindent\rule[.333\baselineskip]{30mm}{.5pt}}

% Folios are placed in the footer
% Headers are used for short chapter names on right page, short thesis title on
% left page
\usepackage{fancyhdr}
\fancypagestyle{plain}{%
  \fancyhead{}
  \fancyfoot{}
  \renewcommand{\headrulewidth}{1pt}
  \renewcommand{\footrulewidth}{0pt}
  \fancyhead[RO]{\nouppercase{\leftmark}}
  \makeatletter
  \fancyhead[LE]{\@shorttitle}
  \makeatother
  \fancyfoot[LE,RO]{\thepage}%
}

% According to UCL guidelines, _ALL_ pages need to have folios
\fancypagestyle{empty}{%
  \fancyhead{}
  \fancyfoot{}
  \renewcommand{\headrulewidth}{0pt}
  \renewcommand{\footrulewidth}{0pt}
  \fancyfoot[LE,RO]{\thepage}%
}

%%
%% Typeset multiple appendices
%% 
\usepackage{appendix}

%% For equation typesetting
\usepackage{amsmath}

%% Typeset XeLaTeX logo, for the colophon
\usepackage{xltxtra}

%%
%% Table of contents
%%
%% As per Bringhurst: "Lists, such as contents pages and recipes, are 
%% opportunities to build architectural structures in which the space between 
%% the elements both separates and binds. The two favourite ways of destroying 
%% such an opportunity are setting great chasms of space that the eye cannot 
%% leap without help from the hand, and setting unenlightening rows of dots 
%% (\emph{dot leaders}, they are called) that force the eye to walk the width of
%% the page like a prisoner being escorted back to its cell." (p.35)
%%
%% Based on his design, the table of contents has page numbers on the left,
%% flush right, and section numbers and titles flush left, with a central dot
%% serving as the separator. Section links reside in the page number (so that 
%% printed in colour this separation becomes clearer).
%%
%% Section hierarchy is marked by font size and style, and additional vertical 
%% keeps section titles from different chapters and parts separate.
%%
%%
\usepackage{titletoc}
\titlecontents{part}
  [0em] % left
  {\addvspace{0.5pc}\raggedright} % above-code
  {\contentsmargin{0pt}\makebox[1.5em][r]{\thecontentspage}\enspace$\cdotp$\enspace\bfseries\Large\thecontentslabel\enspace} % numbered
  {}{} % filler page
\titlecontents{chapter}
  [0em] % left
  {\addvspace{0.5pc}\raggedright} % above-code
  {\contentsmargin{0pt}\makebox[1.5em][r]{\thecontentspage}\enspace$\cdotp$\enspace\large\thecontentslabel\enspace} % numbered
  {}{} % filler page
\titlecontents{section}
  [0em] % left
  {\addvspace{0.25pc}\raggedright} % above-code
  {\contentsmargin{0pt}\makebox[1.5em][r]{\thecontentspage}\enspace$\cdotp$\enspace\thecontentslabel\enspace} % numbered
  {}{} % filler page
\titlecontents{subsection}
  [0em] % left
  {\raggedright} % above-code
  {\contentsmargin{0pt}\makebox[1.5em][r]{\thecontentspage}\enspace$\cdotp$\enspace{\itshape\small\thecontentslabel\enspace}} % numbered
  {}{} % filler page
\titlecontents{figure}
  [0em] % left
  {\addvspace{0.25pc}\raggedright} % above-code
  {\contentsmargin{0pt}\makebox[1.5em][r]{\thecontentspage}\enspace$\cdotp$\enspace\normalsize\thecontentslabel\enspace} % numbered
  {}{} % filler page
\titlecontents{table}
  [0em] % left
  {\addvspace{0.25pc}\raggedright} % above-code
  {\contentsmargin{0pt}\makebox[1.5em][r]{\thecontentspage}\enspace$\cdotp$\enspace\normalsize\thecontentslabel\enspace} % numbered
  {}{} % filler page

%% 
\renewcommand\thepart{\arabic{part}}
  
% As per http://tex.stackexchange.com/a/219823/18982
% To allow a custom part entry in ToC
\usepackage[newparttoc]{titlesec}
\titleformat{\part}[display]
  {\thispagestyle{empty}\Large\bfseries}
  {\partname\nobreakspace\thepart}
  {0mm}
  {\huge\bfseries}
% Standard subsubsection command
\titleformat{\subsubsection}
  {\itshape\large}{\thesubsubsection}{1em}{}

%%
%% Block quotes
%%
\usepackage{setspace}
\usepackage[
  rightmargin = 0pt,
%   vskip       = 0pt,
  leftmargin  = \parindent,
  font+       ={smaller}
]{quoting}
\renewcommand*{\quotingfont}{\setstretch{1}}

%%
%% Notes
%%
%% Sidenotes
\let\oldmarginpar\marginpar
\newcommand{\marginformat}[1]{%
  \setstretch{1}\footnotesize\sffamily#1}
\renewcommand{\marginpar}[1]{%
  \leavevmode\oldmarginpar[\marginformat{\raggedleft#1}]{\marginformat{\raggedright#1}}}
%%
%% Footnotes
%%
%% Once again following Bringhurst, footnotes are typeset with superscripts in
%% the page but full-sized numbers in the note, and the footnote mark hanging
%% from the margin on the left. Implementation as per
%% http://tex.stackexchange.com/a/19845/18982
\usepackage{scrextend}
\deffootnote{0em}{1.6em}{\thefootnotemark\enskip}

\usepackage{caption}
\captionsetup{font=small}
\let\svthefootnote\thefootnote
\newcommand\blankfootnote[1]{%
  \let\thefootnote\relax\footnotetext{#1}%
  \let\thefootnote\svthefootnote%
}

%%
%% Tables
%%

\usepackage{booktabs}
\usepackage{multirow}

% Shortened multi column and row commands
\let\mrow\multirow
\let\mcol\multicolumn

% Horizontally center a single cell in a table
\DeclareExpandableDocumentCommand\crow{ O{1} m }{%
  \multicolumn{#1}{c}{#2}
}

%%
%% Figures
%%

\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{pgfplotstable}
\usetikzlibrary[
  pgfplots.groupplots,
  pgfplots.statistics,
  pgfplots.fillbetween,
  intersections,
  calc,
  shapes.arrows,
  positioning,
  matrix
]

% For subfigures
\usepackage{subfig}

%% Externalise graphics (if the external option is enabled)
%%
\usepgfplotslibrary{external}
\tikzset{
    external/system call={%
    xelatex \tikzexternalcheckshellescape
    -halt-on-error -interaction=batchmode --shell-escape
    -jobname "\image" "\texsource"}}
\ifthenelse{\boolean{bw}}{
  \tikzset{external/prefix={\FIGURES/external/bw_}}
}{
  \tikzset{external/prefix={\FIGURES/external/}}
}
\ifthenelse{\boolean{external}}{\tikzexternalize}{}

%%
%% Fonts
%%
\usepackage{fontspec}
\setmainfont{Linux Libertine O}[
  Numbers={OldStyle,Proportional},
  Mapping=tex-text,
]
\setsansfont{Linux Biolinum O}[
  Mapping=tex-text,
]
\setmonofont{Inconsolata}[
  Mapping=tex-text,
]
\newfontfamily{\monospaced}{Linux Libertine O}[
  Numbers={Monospaced, OldStyle},
  Mapping=tex-text,
]
%% Japanese fonts
\usepackage{xeCJK}
\setCJKmainfont{TakaoMincho}
\setCJKsansfont{TakaoMincho}
\setCJKmonofont{TakaoMincho}

%%
%% Links and metadata
%%
\usepackage[
  colorlinks,
  citecolor=refcolor,
  linkcolor=linkcolor,
  urlcolor=linkcolor,
  linktocpage=true
]{hyperref}

% Set title and author metadata in PDF
\AtEndPreamble{
  \makeatletter
  \hypersetup{
    pdfinfo={
      Author={\@author},
      Title={\@title},
    }
  }
  \makeatother
}

% Go forth the maximal onset rule!
\hyphenation{
  so-cio-lin-guis-tic
}

% Epigraph
\usepackage{epigraph}

% Custom title with short title option
\RenewDocumentCommand\title{ o m }{%
  \makeatletter
  \IfValueTF{#1}{%
    \gdef\@shorttitle{#1}%
  }{
    \gdef\@shorttitle{#2}%
  }%
  \gdef\@title{#2}%
  \makeatother
}

%%
%% Lists and numerations
%%
%% Also based on Bringhurst's lists, the line is flush left with list numbers 
%% placed hanging from the left margin for maximum visibility. No other 
%% punctuation is needed, or used (p.71)
%%

% Typeset inline enumerations in a list environment (see "parablank").
\usepackage[defblank]{paralist}

% Typeset lists
\usepackage{enumitem}
\setlist[enumerate]{
  leftmargin=0pt,
  rightmargin=0pt,
%   rightmargin=\parindent,
  nosep,
  label=\arabic*,
}
\setlist[enumerate,2]{
  rightmargin=0pt,
  leftmargin=0pt,
  label*=.\arabic*,
}
\setlist[enumerate,3]{
  rightmargin=0pt,
  leftmargin=0pt,
  label*=.\arabic*,
}

%%
%% New commands
%%

%% Custom cleardoublepage command
%%
%% Pages after titles that clear a double page should use an empty pagestyle
%% (although because of the UCL guidelines, the empty pagestyle _also_ has page
%% numbers).
%%
\makeatletter
\renewcommand{\cleardoublepage}{%
  \clearpage
  \if@twoside
    \ifodd\c@page\else
      \hbox{}%
      \thispagestyle{empty}
      \newpage
      \if@twocolumn
        \hbox{}
        \newpage
      \fi
    \fi
  \fi
}
\makeatother

%% Custom maketitle command (copied from ucl_thesis class)
%%
%% Do not italicise author name
%%
\makeatletter
\renewcommand \@maketitle{%
  \newpage%
  \null%
  \vspace*{5em}%
  \begin{center}%
    {\huge \bfseries \@title}\\[5em]%
    {\Large \@author}\\%
  \end{center}%
  \vfill%
  \begin{center}%
  A dissertation submitted in partial fulfillment \\
  of the requirements for the degree of \\
  \textbf{\@degree@string} \\
  of \\
  \textbf{University College London}
  \end{center}%
  \vspace{2em}%
  \begin{center}%
  \@department \\
  University College London\\
  \end{center}%
  \vspace{2em}%
  \begin{center}%
  \@date%
  \end{center}%
  \if@twoside %
    \newpage%
    ~\\
    \newpage%
  \fi
}
\makeatother

%% Custom declaration command (copied from ucl_thesis class)
%%
%% Do not indent paragraph, and do not clear \@author macro
%%
\makeatletter
\renewcommand \makedeclaration {%
  \clearpage%
  \noindent I, \@author, confirm that the work presented in this thesis is my own.
  Where information has been derived from other sources, I confirm that this has been indicated in the work.
  \clearpage%
}
\makeatother

%% Links to arbitrary parts of text
%%
%% Used as \anchor{label}, a reference to that specific part of the text can
%% be made as if it was a normal section title or float caption
%% (eg. \ref{label})
%%
\newcommand{\anchor}[1]{\phantomsection\label{#1}}

%% TiKZ nodes in arbitrary parts of text
%%
\newcommand{\tikzmark}[1]{%
  \tikz[overlay,remember picture,baseline] \node [anchor=base] (#1) {};%
}

%% Typeset a color sample in a circle
%%
%% Inspired by the xcolor package documentation. Use like \inlineclr{colorname}
%%
\newcommand{\inlineclr}[1]{%
  \tikzset{external/export next=false}%
  \tikz[baseline=0.25ex] \fill[#1] (1ex,1ex) circle (1ex);%
}

% Super and subscript commands, as per 
% http://anthony.liekens.net/index.php/LaTeX/SubscriptAndSuperscriptInTextMode
%%
\usepackage{relsize}
\newcommand{\superscript}[1]{\ensuremath{^{\textrm{\smaller[2]#1}}}}
\newcommand{\subscript}[1]{\ensuremath{_{\textrm{\smaller[2]#1}}}}

%% Typeset ordinal numbers
%%
\usepackage[super]{nth}
\newcommand{\first} {\nth{1}}
\newcommand{\second}{\nth{2}}
\newcommand{\third} {\nth{3}}
\newcommand{\fourth}{\nth{4}}
\newcommand{\fifth} {\nth{5}}

%% Typeset foreign words and common abbreviations (eg, "eg")
%%
\usepackage[british,abbreviations]{foreign}
\newcommand{\versus}{\foreign{v.}}

%% Draw a fleuron
%%
\newcommand{\fleuron}{\begin{center}\includegraphics{fleuron.pdf}\end{center}}

%%
%% General macros
%%

\newcommand{\Tokyo}{Tōkyō}
\newcommand{\Kanto}{Kantō}
\newcommand{\Nonquestion}{Declaration}
\newcommand{\Question}{Interrogation}
\newcommand{\UtteranceFinal}{Final}
\newcommand{\UtteranceNonfinal}{Non-final}
\newcommand{\SpanishFirst}{Proparoxytone}
\newcommand{\SpanishMiddle}{Paroxytone}
\newcommand{\SpanishLast}{Oxytone}
\newcommand{\JapaneseFirst}{First}
\newcommand{\JapaneseMiddle}{Last}
\newcommand{\JapaneseLast}{Unaccented}
\newcommand{\Natives}{Natives}
\newcommand{\tandemstraight}{\textsc{tandem-straight}}

\newcommand{\cvstyle}[1]{\textsc{#1}}
\newcommand{\consonant}{\cvstyle{c}}
\newcommand{\vowel}{\cvstyle{v}}
\newcommand{\nasal}{\cvstyle{n}}
\newcommand{\glide}{y}
\newcommand{\geminate}{\cvstyle{q}}
\newcommand{\longvowel}{\cvstyle{r}}
\newcommand{\figurewidth}{\linewidth}

%% For typesetting scientific units
%%
\usepackage[binary-units=true, per-mode=symbol]{siunitx}
% Declare missing "semitone" unit
\DeclareSIUnit\semitone{st}

\newcommand{\blank}{\underline{\hphantom{blank}}}
\newcommand{\percent}{\si{\percent}}
\newcommand{\kbps}{\si{\kilo\byte\per\second}}
\newcommand{\mora}{\ensuremath{\mu}}
\newcommand{\fzero}{\textsc{f}$_0$}
\newcommand{\fone}{\textsc{f}$_1$}
\newcommand{\ftwo}{\textsc{f}$_2$}
\newcommand{\fthree}{\textsc{f}$_3$}
\newcommand{\fall}{\ensuremath{^{\downarrow}}}
\newcommand{\rise}{\ensuremath{^{\uparrow}}}

%% Typeset ToBI notation
%%
\newcommand{\high}{\textsc{h}}
\newcommand{\low}{\textsc{l}}
\newcommand{\Lstar}{\low\kern-0.465em\raisebox{0.15em}{*}\kern0.09em}
\newcommand{\Hstar}{\high\kern-0.485em\raisebox{0.15em}{*}\kern0.11em}
\newcommand{\ToBIminus}{-}
\newcommand{\ToBIplus}{+}
\newcommand{\ToBIdowntrend}{\raisebox{0.15em}{\smaller¡}}
\newcommand{\ToBIforward}{\kern-0.15em\raisebox{0.02ex}{>}}
\newcommand{\ToBIboundary}{\textsc{\%}}
% ToBI wrappers
\newcommand{\LHstar}{\mbox{\low\ToBIplus\Hstar}}
\newcommand{\LhiHstar}{\mbox{\low\ToBIplus\ToBIdowntrend\Hstar}}
\newcommand{\LfwHstar}{\mbox{\low\ToBIplus\ToBIforward\Hstar}}
\newcommand{\LstarH}{\mbox{\Lstar\ToBIplus\high}}
\newcommand{\HLstar}{\mbox{\high\ToBIplus\Lstar}}
\newcommand{\HstarL}{\mbox{\Hstar\ToBIplus\low}}
\newcommand{\Hboundary}{\mbox{\high\ToBIboundary}}

%%
%% Figure commands
%%

%% Plot a vertical line across entire plot. Call with a comma-separated list of
%% x-coordinates. PGF options can be passed as an optional argument.
%%
\DeclareDocumentCommand\plotvline{ O{} m }{%
  \def\temparg{#1}
  \pgfplotsforeachungrouped \x in {#2} {
    \edef\temp{\noexpand\draw[\temparg]
      ({axis cs:\x,\noexpand\pgfkeysvalueof{/pgfplots/ymin}}) --
      ({axis cs:\x,\noexpand\pgfkeysvalueof{/pgfplots/ymax}});}
    \temp
  }
}

%% Plot a horizontal line across entire plot. Call with a comma-separated list of
%% y-coordinates. PGF options can be passed as an optional argument.
%%
\DeclareDocumentCommand\plothline{ O{} m }{%
  \def\temparg{#1}
  \pgfplotsforeachungrouped \y in {#2} {
    \edef\temp{\noexpand\draw[\temparg]
      ({axis cs:\noexpand\pgfkeysvalueof{/pgfplots/xmin},\y}) --
      ({axis cs:\noexpand\pgfkeysvalueof{/pgfplots/xmax},\y});}
    \temp
  }
}

%% Typeset a blank boxplot, useful for separating boxplots
%%
\DeclareDocumentCommand\blankboxplot{ O{} G{0} }{%
  \addplot[
    opacity=0,
    boxplot/draw/box/.code={},
    boxplot/draw/median/.code={},
    boxplot prepared={
      lower whisker = #2,
      lower quartile = #2,
      median = #2,
      upper quartile = #2,
      upper whisker = #2,
    }, #1
  ] coordinates {};
}

%%
%% Statistical report commands
%%

%% Report a p value. Call as \pvalue{<0.001}
%%
\DeclareDocumentCommand\pvalue{ g }{%
  \mbox{\ensuremath{p}%
  \IfValueTF{#1}{%
    \ensuremath{#1}%
  }{}}%
}

%% Report a standard deviation. Call as \sd for the standard deviation symbol
%% (a lowercase sigma), or \sd{x} for typesetting the value. An optional value 
%% can be used for a subscripted explanation (eg, sd$_{old}$).
%%
\DeclareDocumentCommand\sd{ o g }{%
  \IfValueTF{#2}%
    {%
      \mbox{%
        \ensuremath{\sigma}%
        \IfValueTF{#1}{\ensuremath{_{\mathrm{#1}}}}{}%
        \ensuremath{=#2}%
      }%
    }
    {\ensuremath{\sigma}}%
}

%% Report a mean. Call as \mean for the mean symbol (a lowercase x with a
%% horizontal bar on top), as \mean{x} for typesetting the value as a mean, or
%% as \mean{x}{y} for typesetting a mean (x) and a standard deviation (y). An 
%% optional value can be used for a subscripted explanation (eg, mean$_{old}$).
%%
\DeclareDocumentCommand\mean{ o g g }{%
  \IfValueTF{#2}%
    {%
      \mbox{%
        \ensuremath{\bar{x}}%
        \IfValueTF{#1}{\ensuremath{_{\mathrm{#1}}}}{}%
        \ensuremath{=#2}%
      }%
      \IfValueTF{#3}{, \sd{#3}}{}%
    }
    {\ensuremath{\bar{x}}}%
}

%% Report a chi-squared result. Call as \chisq for the chi-squared symbol (an
%% uppercase chi with a superscript 2), or as \chisq{x}{y}{z} for typesetting 
%% the complete result, where x is the degrees of freedom, y the chi-squared 
%% result, and z the p-value.
%%
\DeclareDocumentCommand\chisq{ o g g g }{%
  \IfValueTF{#4}%
    {\mbox{\ensuremath{\chi^2_{(\IfNoValueF{#1}{\mathrm{#1,~}}#2)}=#3}}, \pvalue{#4}}%
    {\IfValueTF{#1}{\ensuremath{\chi^2_{\mathrm{#1}}}}{\ensuremath{\chi^2}}}%
}

%% Report an F result. Call as \fvalue for the F symbol (an uppercase letter F),
%% or as \fvalue{x}{y}{z} for typesetting the complete result, where x is the 
%% degrees of freedom, y the test result, and z the p-value.
%%
\DeclareDocumentCommand\fvalue{ o g g g }{%
  \IfValueTF{#4}%
    {\mbox{\ensuremath{F_{(\IfNoValueF{#1}{\mathrm{#1,~}}#2)}=#3}}, \pvalue{#4}}%
    {\IfValueTF{#1}{\ensuremath{F_{\mathrm{#1}}}}{\ensuremath{F}}}%
}

%% Report the result of a t-test. Call as \ttest for the test symbol (a 
%% lowercase letter t), or as \ttest{x}{y}{z} for typesetting the complete 
%% result, where x is the degrees of freedom, y the test result, and z the 
%% p-value.
%%
\DeclareDocumentCommand\ttest{ o g g g }{%
  \IfValueTF{#4}%
    {\ensuremath{t_{(\IfNoValueF{#1}{\mathrm{#1,~}}#2)}=#3}, \pvalue{#4}}%
    {\IfValueTF{#1}{\ensuremath{t_{\mathrm{#1}}}}{\ensuremath{t}}}%
}

%% Input paths
% These are redefined in each section for local paths
\newcommand{\TABLES}{}
\newcommand{\FIGURES}{}
\newcommand{\DATA}{}

%% Manage paths for figures, data, and tables
%%
%% Call this command as \setroot{chapter/root}, and this
%% will automatically update the other path macros to point to
%% the appropriate directories.
%%
\pgfkeys{
  /my/root/.store in=\ROOT,
  /my/figures/.store in=\FIGURES,
  /my/data/.store in=\DATA,
  /my/tables/.store in=\TABLES,
  /my/root=.,
  /my/figures={\ROOT/figures},
  /my/data={\ROOT/data},
  /my/tables={\ROOT/tables},
}
\newcommand{\setroot}[1]{\pgfkeys{/my/root=#1}}

%%
%% Bibliography
%%

\usepackage[
  backend=biber,
  style=authoryear-comp,
  sorting=nyt,
  language=english,
  backref=true,
  backrefstyle=three+,
  sortcites=false,
  natbib,
%   refsection=chapter,
  maxbibnames=99,
  uniquename=false,
]{biblatex}

%% Define citation aliases
\defcitealias{limesurvey}{LimeSurvey}
\defcitealias{tandemstraight}{TandemSTRAIGHT}
\defcitealias{r}{\texttt{R}}
\defcitealias{praat}{Praat}
\defcitealias{lme4}{\texttt{lme4}}
\defcitealias{car}{\texttt{car}}
\defcitealias{lifcach}{\textsc{Lifcach}}

%%
%% Colour schemes
%%

%%
%% Define custom color scheme
%%

\pgfkeys{
  /colour/.is family, colour,
}
\newcommand{\newcolor}[4]{%
  \IfStrEq{#3}{mix}{
    \colorlet{#1}{#4}
  }{
    \definecolor{#1}{#3}{#4}
  }
  \pgfkeys{colour, #1/.is family, #1, value/.code=#4, name/.code=#2}
}
\newcommand{\colorname}[1]{\pgfkeys{colour, #1, name}}
\newcommand{\showcolor}[1]{\protect\colorname{#1}~\protect\tikzsetfigurename{inlineclr}\inlineclr{#1}}

\ifthenelse{\boolean{printer}}{%
  \newcolor{refcolor} {black} {named}{black}
  \newcolor{linkcolor}{black} {named}{black}
  \newcolor{interval} {gray}  {gray} {0.8}
}{
  \newcolor{refcolor} {blue}  {HTML} {0F4DA8}
  \newcolor{linkcolor}{purple}{HTML} {840084}
  \newcolor{interval} {gray}  {gray} {0.8}
}

\ifthenelse{\boolean{bw}}{%
  %% Black and white colour scheme (used if the bw option is set)
  \newcolor{chance}   {gray}      {gray} {0.5}

  \newcolor{two1}     {black}     {named}{black}
  \newcolor{two2}     {gray}      {mix}  {black!50}

  \newcolor{alttwo1}  {black}     {named}{two1}
  \newcolor{alttwo2}  {gray}      {named}{two2}

  \newcolor{three1}   {black}     {named}{black}
  \newcolor{three2}   {gray}      {mix}  {black!66}
  \newcolor{three3}   {light gray}{mix}  {black!33}

  \newcolor{four1}    {black}     {named}{black}
  \newcolor{four2}    {dark gray} {mix}  {black!75}
  \newcolor{four3}    {gray}      {mix}  {black!50}
  \newcolor{four4}    {light gray}{mix}  {black!25}

  \newcolor{five1}    {black}     {named}{black}
  \newcolor{five2}    {dark gray} {mix}  {black!80}
  \newcolor{five3}    {gray}      {mix}  {black!60}
  \newcolor{five4}    {light gray}{mix}  {black!40}
  \newcolor{five5}    {lightest gray}{mix}  {black!20}

  \newcolor{chilean}  {black}     {named}{two1}
  \newcolor{japanese} {gray}      {named}{two2}
  \newcolor{fzero}    {black}     {named}{two1}

  \pgfplotscreateplotcyclelist{custom}{%
    four1, color=four1, fill=four1, solid \\%
    four2, color=four2, fill=four2, solid \\%
    four3, color=four3, fill=four3, solid \\%
    four4, color=four4, fill=four4, solid \\%
  }

  \newcolor{listbackground}   {}  {gray} {0.25}
  \newcolor{listcomment}      {}  {gray} {0.2}
  \newcolor{listcontent}      {}  {named}{black}

}{
  %% Full colour scheme (used by default, or if the color option is set)
  \newcolor{solarised_yellow} {yellow}       {HTML} {DCA700}%{B58900}
  \newcolor{solarised_orange} {orange}       {HTML} {CB4b16}
  \newcolor{solarised_red}    {red}          {HTML} {DC322F}
  \newcolor{solarised_magenta}{magenta}      {HTML} {D33682}
  \newcolor{solarised_violet} {violet}       {HTML} {6C71C4}
  \newcolor{solarised_blue}   {blue}         {HTML} {268BD2}
  \newcolor{solarised_cyan}   {cyan}         {HTML} {2AA198}
  \newcolor{solarised_green}  {green}        {HTML} {859900}

  \newcolor{solarised_base03} {darkest gray} {HTML} {002B36}
  \newcolor{solarised_base02} {dark gray}    {HTML} {073642}
  \newcolor{solarised_base01} {gray}         {HTML} {586E75}
  \newcolor{solarised_base00} {gray}         {HTML} {657B83}
  \newcolor{solarised_base0}  {gray}         {HTML} {839496}
  \newcolor{solarised_base1}  {gray}         {HTML} {93A1A1}
  \newcolor{solarised_base2}  {light gray}   {HTML} {EEE8D5}
  \newcolor{solarised_base3}  {lightest gray}{HTML} {FDF6E3}

  \newcolor{two1}             {red}    {named}{solarised_red}
  \newcolor{two2}             {blue}   {named}{solarised_blue}

  \newcolor{alttwo1}          {yellow} {named}{solarised_yellow}
  \newcolor{alttwo2}          {green}  {named}{solarised_green}

  \newcolor{three1}           {cyan}   {named}{solarised_cyan}
  \newcolor{three2}           {green}  {named}{solarised_green}
  \newcolor{three3}           {yellow} {named}{solarised_yellow}

  \newcolor{four1}            {yellow} {named}{solarised_yellow}
  \newcolor{four2}            {red}    {named}{solarised_red}
  \newcolor{four3}            {violet} {named}{solarised_violet}
  \newcolor{four4}            {cyan}   {named}{solarised_cyan}

  \newcolor{five1}            {yellow} {named}{solarised_yellow}
  \newcolor{five2}            {red}    {named}{solarised_red}
  \newcolor{five3}            {violet} {named}{solarised_violet}
  \newcolor{five4}            {cyan}   {named}{solarised_cyan}
  \newcolor{five5}            {blue}   {named}{solarised_blue}

  \newcolor{chilean}          {red}    {named}{two1}
  \newcolor{japanese}         {blue}   {named}{two2}
  \newcolor{fzero}            {blue}   {named}{two2}

  \newcolor{chance}           {violet} {named}{solarised_violet}

  \newcolor{listbackground}   {}       {named}{solarised_base3}
  \newcolor{listcomment}      {}       {named}{solarised_base1}
  \newcolor{listcontent}      {}       {named}{solarised_base00}

  \pgfplotscreateplotcyclelist{custom}{%
    solarised_blue,    color=solarised_blue,    fill=solarised_blue,    solid \\%
    solarised_red,     color=solarised_red,     fill=solarised_red,     solid \\%
    solarised_green,   color=solarised_green,   fill=solarised_green,   solid \\%
    solarised_violet,  color=solarised_violet,  fill=solarised_violet,  solid \\%
    solarised_orange,  color=solarised_orange,  fill=solarised_orange,  solid \\%
    solarised_cyan,    color=solarised_cyan,    fill=solarised_cyan,    solid \\%
    solarised_magenta, color=solarised_magenta, fill=solarised_magenta, solid \\%
    solarised_yellow,  color=solarised_yellow,  fill=solarised_yellow,  solid \\%
  }
}

%% Define pgfplots colour cycle lists for specific colour contrasts

%% Two-colour contrast
\pgfplotscreateplotcyclelist{two}{%
  two1, color=two1, fill=two1, solid \\%
  two2, color=two2, fill=two2, solid \\%
}

%% Use spanish colour first, then japanese
\pgfplotscreateplotcyclelist{spanish}{%
  color=chilean,  fill=chilean,  solid \\%
  color=japanese, fill=japanese, solid \\%
}

%% Use japanese colour first, then spanish
\pgfplotscreateplotcyclelist{japanese}{%
  color=japanese, fill=japanese, solid \\%
  color=chilean,  fill=chilean,  solid \\%
}

%% Three-colour contrast
\pgfplotscreateplotcyclelist{three}{%
  three1, color=three1, fill=three1, solid \\%
  three2, color=three2, fill=three2, solid \\%
  three3, color=three3, fill=three3, solid \\%
}

%% Four-colour contrast
\pgfplotscreateplotcyclelist{four}{%
  four1, color=four1, fill=four1, solid \\%
  four2, color=four2, fill=four2, solid \\%
  four3, color=four3, fill=four3, solid \\%
  four4, color=four4, fill=four4, solid \\%
}

%%
%% Code listings
%%

\usepackage{listings}
\lstset{%
  basicstyle=\color{listcontent}\scriptsize\ttfamily,
  commentstyle=\color{listcomment},
  backgroundcolor=\color{listbackground},
  moredelim=**[is][\upshape]{`}{`},
  tabsize=2,
  gobble=2,
  numbers=left,
  numberstyle=\color{listcomment}\tiny,
  showtabs=false,
  captionpos=b,
}

%%
%% Glossaries
%%
\usepackage[hyperfirst=false,nonumberlist,]{glossaries}
\renewcommand{\glsgroupskip}{}

% Programmaticaly set first letter to uppercase
\usepackage{mfirstuc}

% Capitalise first letter in the description
% As per http://tex.stackexchange.com/a/79910/18982
\let\firstchar\lowercase
\let\oldprintglossaries\printglossaries
\def\printglossaries{\let\firstchar\uppercase\oldprintglossaries\let\firstchar\lowercase}

%% Typeset a long version of the abbreviation 
%% (eg. "Free Software Foundation (FSF)")
\DeclareDocumentCommand\glslong{ s m } {%
  \IfValueTF{#1}{%
    \glsdesc*{#2} (\glstext*{#2})%
  }{%
    \glsdesc{#2} (\glstext{#2})%
  }%
}

%% Typeset a long version of the abbreviation (forcing an initial uppercase 
%% letter; eg. "Free Software Foundation (FSF)")
\DeclareDocumentCommand\Glslong{ s m } {%
  \IfValueTF{#1}{%
    \Glsdesc*{#2} (\glstext*{#2})%
  }{%
    \Glsdesc{#2} (\glstext{#2})%
  }%
}

%% Generate glossaries file
\makeglossaries

%%
%% References
%%

%% Cleveref loaded last, as per the documentation
%%
\usepackage[noabbrev]{cleveref}
