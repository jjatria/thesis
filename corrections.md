# Joint report

[x] Improve review of earlier experimental studies on lexical stress in Spanish.
    **Fixed**: The section in the introduction detailing the acoustic cues of
    the Spanish prominence has been expanded to include a more detailed revision
    of the literature and the discussion within it, and a more critical
    appraisal of the studies mentioned: 0699eaf

[x] (Minor) Rather than refer to reviews, provide more specific references in 
    some cases: eg. Enríquez, Casado and Santos (1989) is relevant for cue 
    trading relationships.
      [×] Enríquez, Casado and Santos (1989)
          F0 is most important, duration less so. Intensity only marginal.
          PPO > OXY > POX, maybe due to methodological issues?
          As critique: No downtrend in synthetic stimuli.
      [×] Quilis (1983)
          Paroxytone words four times as common as the rest in stressed words.
    **Fixed**: 0699eaf, bc8267f

[x] (Minor) Additional references for Spanish peak displacement:
      [x] Llisterri, Marín, de la Mota, and Ríos (1995)
          Peak displacement found in 80% of cases, but never across
          NP subject+VP boundaries, and less commonly before pauses
      [x] Garrido, Llisterri, de la Mota, and Ríos (1993)
          80% of stressed syllables did not align
      [x] Prieto, van Santen, and Hirschberger (1995)
          Two main factors affecting peak displacement: segmental duration,
          and surrounding prosodic structure
      *   etc.
    **Fixed**: Added more detailed coverage of the rate of occurence and factors
    involved: 8609d4b

[x] (Minor) Provide bibliographical references for statements regarding basic
    features of lexical stress
      [x] p83: ``Duration is one of the three main acoustic cues of Spanish''
      [x] p94: ``Traditionally, pitch has been described as the only meaningful
               acoustic cue of the Japanese accent''
    **Fixed**: 0699eaf, bc8267f, c73fb49

[x] (Minor) Additional references for Chilean Spanish lexical stress research:
      [x] Ortiz Lira (2003)
          Looked at casual speech by Santiago speakers and found a predominance
          of H*+L prenuclear accents. He mentions that L*+H accents in Chile
          "contribute greatly to the perception of speakers as foreigners".
          Cf. with my results.
      [x] Ruiz Mella, and Pereira (2010)
          Similar results to those shown by Llisterri and colleagues: F0 is the
          main cue for prominence, with duration as a secondary. They reference
          Urrutia (2007) who promotes the role of intensity, but they find
          otherwise.
          As a critique: clumsy use of semitones and acoustic measures.
      [x] Toledo and Astruc ( 2008)
          Neither in Chile nor in Bs.As. are there any pitch accents with a
          H following the accented syllable.
          Just mention this article. It's not of great use to me.
      *   etc.
    **Fixed**: Added references to more studies on Chilean Spanish in
    particular, including a brief discussion of evidence that might support
    a difference in the distribution of peak displacement for the Chilean
    variety: 0869a46

[x] More exhaustive review of work on perception of L2 lexical stress in other
    languages.
    **Fixed**: Added review of literature for stress deafness and L2 perception
    of prominence: 3ec0b5f

[x] (Minor) Additional review of perception of L2 lexical stress in other
    languages would be welcome.
      [x] Muñoz García (2010) and Muñoz García, and Baqué (2009): production and
          perception of Spanish lexical stress by French speakers in declarative
          and interrogative utterances.
            * Perception of lexical accent decreases as a function of the
              participant's language level
            * Perception in rising intonation were more difficult
            * Perception of oxytone accents is least affected by rising
              intonation
            * Significant effects of word-length and accent pattern
    **Fixed**: 3ec0b5f

[x] (Minor) p144: Evidence for low-level acoustic processing in an
    identification task has also been provided in the case of the perception of
    lexical stress in Spanish by French speakers:
      [x] Dupoux, Pallier, Sebastián Gallés, and Mehler (1997)
      [x] Schwab, and Llisterri (2010, 2011)
    **Fixed**: 3ec0b5f

## Linguistic questionnaires

[x] Increase detail of justification of self-assessment questionnaires for
    language proficiency.
    [x] Li, Sepanski, Zhao (2006)
        * Based on a review of 41 papers using linguistic questionnaires, a
        generic web-based version was developed with the most common questions.
        This version of the LHQ has been validated by separate studies.
    [x] Li, Zhai, Tsang, Puls (2014)
        * Building upon the previous version, a new modular and dynamic version
        of the LHQ offers increased versatility and privacy, as well as numerous
        other smaller fixes.
    **Fixed**: Linguistic questionnaires have been used extensively in
    bilingualism research, and have been established as a reliable tool. It is
    important that researchers are aware of their limitations, but they offer a
    good balance between the information gathered regarding the participants'
    profiles, and the experimental load to which these participants are subject.
    This has been added to the introduction, and the discussion regarding cloze
    tests was moved here as well: f751d2b

[ ] Report extra background work done by myself (?)

## Online tests

[x] Justify the use of online tests
    **Fixed**: added additional bibliographical references discussing the
    validity of online data in comparison with more traditional laboratory data,
    and discussed the reasons motivating the decision to use online tests in
    the experiments in part 1: 331330a

[x] Discuss the effects that online tests might have had on the results.
    **Fixed**: Based on literature, online data is possibly more variable and
    has smaller statistical power: 331330a

[x] Discuss the effects of online tests on the interpretation of the work.
    **Fixed**: I don think other sources of data will have a significant effect
    on the results, since data variance is comparable with original study.
    However, I added mention that other sources of data (together with broader
    participant recruitment) might shed light on e.g. the lack of L2 proficiency
    effects (because of the possible smaller statistical power): 331330a

[x] On page 167, participants were not supervised. This might lead to poor data.
    **Fixed**: Participants in the experiments of part 2, which is being
    reported in this chapter, were supervised, unlike those of the experiments
    in part 1. The alluded sentence makes reference to the participants from
    part 1, and a clarifying sentence has been added to stress this difference:
    91d62ec

## Additional data

[ ] Provide audio illustrations of samples.
    **TODO**: on website. This is halfway done already.
      [ ] Fix URLs

[x] (Minor) Provide additional data and comments on the overall frequency of
    proparoxytone, paroxytone and oxytone words to show prevalence of paroxytone
    words in Spanish.
    **Fixed**: 0699eaf, bc8267f

## F0 analysis

[x] Justify time normalisation in f0 curves.
      [x] Lucero, Munhall, Gracco & Ramsay (1997)
    **Fixed**: The method suggested by Lucero and colleagues is particularly
    beneficial for longer stretched of speech, and they themselves claim that
    the assumptions of linear normalisation are sound for short stretches.
    This meant that there was probably little to gain from the implementation
    of their significantly more complex normalisation method. This explanation
    has been added as a footnote to the text: 99bd848

[x] Why ToBI? Justify the use of a phonological annotation framework.
    **Fixed**: The justification in the introduction was briefly expanded, and a
    disclaimer was added to the chapter on acoustic analysis of the stimuli,
    explaining why ToBI was used, and that despite its use, no claims are made
    regarding the phonological status of the tones described. ToBI is not so
    much a phonological annotation framwork, but a system for prosodic
    annotation, and that there is a precedent for its use in the description of
    intonation at a level that is above that of the phonology, and equivalent to
    the IPA for segments (which incidentally is the goal stated for the system
    in its foundational paper): 8ac3a56

[ ] (Minor) Discuss the issues arising from the phonological rather than
    phonetic nature of the ToBI model. ``The notion that Spanish has 'pitch
    accents' and the disctinction between 'stress' and 'accent' might be a
    consequence of the model''.

[ ] (Minor) Acoustic analysis of duration and intensity is phonetic, analysis of
    intonation is phonological.

## Theory and extensions

[ ] Goals in chapter 1 and 3 are very empirical. Can they address theoretical
    issues raised in the introduction (eg. compare between SLM and PAM)? If not,
    discuss what would be a satisfactory approach.

[ ] Discuss in more detail the performance of control groups.

[ ] Compare their performance with that of previous studies.

[x] Discuss why /momo/ behaves in a manner unlike the rest.
    **Fixed**: This is likely because the contrast involved in /momo/ is more
    salient than the contrast in the rest of the Japanese pairs, since it
    involves the first and unaccented patterns. If this is the case, then it
    should be tested by looking at auditory discrimination. cb50d2d

## Cross-language studies

[x] Why no tests that used language group as a factor for bidirectional
    comparison? Justify design.
    **Fixed**: Added a section in the closing regarding methodological
    limitations, and explaining why no cross-linguistic direct comparisons were
    made. This is no presented as an admission that the methodology is flawed,
    but as a cautionary tale regardig the application and design of what seems
    to be a difficult but rewarding methodology:

[ ] (Minor) Results from the control group are only presented in figure 3.3(a)
    and (b). More detailed discussion would be welcome.

## Limitations

[x] Recruitment of participants should be equivalent for all groups. Some were
    recruited using different methods. Mention as a limitation.
    **Fixed**: Mentioned limitation in chapter 2, regarding the first Spanish
    study. Mentioned it again in chapter three, regarding the first Japanese
    study, but making a reference to the discussion in the preceding chapter,
    pointing out that the same issues applied in both cases:

[ ] Use or morphing software limited the manipulation of intensity and was more
    successful at one end of the continua than the other. This might affect
    results. Consider and discuss.

[ ] Boundary positions were calculated at 50%, but not all participants passed
    that threshold, meaning missing data. At the very least mention as a
    limitation. Discuss use of other thresholds: eg. 75% (Hirsh, 1959).

[ ] Instead of using category slopes, extent of continuum range that resulted in
    given levels of categorisation might be a better statistic.

[ ] Why wasn't discrimination tested?
    * sec:second_language_acquisition
      dupoux() mentions that discrimination tasks result in disproportionately
      higher performance rates because participants have access to acoustic
      representations instead of phonological ones (it is not a linguistic task).

[ ] Ceiling effects in Kimura et al. (2012) and own native data can cause
    spurious interactions. Although no language group comparisons were measured,
    discuss this limitation.

[ ] (Minor) Acoustic analysis could have been compared with those from previous
    work in Spanish and Japanese, to reinforce suitability of materials for
    subsequent experiments.

## Language teaching

[x] Discuss in more detail the teaching of suprasegmentals for both languages
    as an extra section in final chapter.
    **Fixed**: Added considerably more references and expanded the discussion
    of the teaching of suprasegmentals as a second language. No extra section
    was added to the final chapter since the section where teacher attitudes
    were discussed originally already seemed like the natural place to have
    this, but this section is greatly expanded: 5b745db

[x] (Minor) Additional references regarding teacers' attitudes towards
    pronunciation teaching in Spanish
      [x] Orta (2009)
      [x] Orta 2010)
      [ ] Usó (2009)
    **Fixed**: 5b745db

[x] (Minor) Additional reference for difficulties of Japanese learners of
    Spanish:
      [x] Carranza, Cucchiarini, Llisterri, Machuca, and Ríos (2014)
    **Fixed**: 0845c5f, but their results are exclusively about segmental
    errors.

[x] (Minor) Additional references regarding teaching of suprasegmentals in
    Spanish L2 based on manipulation of acoustic parameters
      [x] Lahoz (2011)
      [x] Lahoz (2012)
      [x] Corominas Pérez (2014)
      [x] Saalfeld (2012), for the effects of training on Spanish lexical stress
    **Fixed**: 5b745db

## Clarifications

[x] Clarify location of Cuenca
    **Fixed**: Replaced with ``the Spanish province of Cuenca''.

[x] Clarify in text that ``fifty one students of spanish residents in Japan''
    refers to someone else's study.
    **Fixed**: Explicitly stated what study the sentence was about.

[x] Occasional loose usage of ``significance'' (eg. p.177)
    **Fixed**: Reserved use of ``significant'' and derived words for the
    evaluation of statistical results throughout.

[ ] Clarify in text what participants should do (from page 102 onwards).

[x] p79: ``largely constricted to the word'' =~ s/constricted/restricted/

[x] Figure 1.2: ``takara'' and ``sakana'' seem to have the same pitch contour.
    **Fixed**: This is the point of the figure. This lack of contrast between
    isolated words has been explicitly stated in the caption.

# Minor changes

[x] (Minor) Present a more critical discussion of the methodology in
    Ortega-Llebaria and Prieto (2011). ``The absence of pitch''.
    **Fixed**: The footnote containing the critique has been expanded and moved
    to the main body of the page: 0699eaf

[x] (Minor) Questions regarding the self-assessment questionnaire
      [x] Question 11 shows proficiency level answers but rate of use questions
          **Fixed**: This was a typing error. It has been corrected.
      [x] Asking frequency of use ``for yourself'' might not be clear
          **Fixed**: Changed to ``by yourself''. A clarification was also added
          to the first description of the questionnaire, in the introduction
    **Fixed**: 1dbefaa
      [ ] An addition of questions regarding type of institution might be useful

[ ] (Minor) p92: Is final lengthening also part of the prototypical example?

[x] (Minor) Confusing notions of pitch and fundamental frequency:
      [x] p94: ``Traditionally, pitch has been described as the only...''
      [x] p96: ``In the case of Spanish, lexical prominence has three main
          acoustic correlates''
    See entries from
      [ ] Crystal (2008)
      [ ] Trask (1996)
    **Fixed** Separation between the notions of pitch and fundamental frequency
    was checked throughout: 377d83c6, f12b9a61, 924c9a27

[ ] (Minor) Bias towards paroxytone responses in Chapter 3 might be due to
    paroxytone being the unmarked stress pattern in Spanish (eg. p107, p116).
    **TODO**: no.

[x] (Minor) Clarify that following figures only show data from non-native
    participants.
    **Fixed**: b6c0657

[x] (Minor) ``Maybe the phonological factors [affecting the word-initial bias
    for {sj}] are related to the unmarked paroxytone pattern in Spanish''.
    **Fixed**: As discussed in the viva, this is unlikely, since this would
    require parsing the 2-syllable word as separate from the particle. Are SJ
    doing this? More studies needed: ef8c45e

[x] (Minor) Missing information in bibliography
    **Fixed**: 6f764036

[x] (Minor) Minor typos
